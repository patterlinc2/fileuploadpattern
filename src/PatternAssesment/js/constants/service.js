const baseUrl = 'http://localhost:3000/';

const service = {
	questionMetaData: baseUrl + 'questionMetaData',
	saveQuestionMetaData: baseUrl + 'saveQuestionMetaData',
	autoCompleteData:baseUrl +'autoCompleteData',
	assessmentMetaData:baseUrl +'assessmentMetaData'

}

module.exports = service;
