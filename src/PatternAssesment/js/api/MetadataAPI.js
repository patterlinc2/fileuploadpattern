import request from 'superagent-bluebird-promise'
import service from '../constants/service'
const TIMEOUT = 100

export default {

    get_QMD_Data() {
    return request.get(service.questionMetaData).promise();
  },

      save_QMD_Data() {
    return request.get(service.saveQuestionMetaData).promise();
  },
        get_AutoComplete_Data() {
    return request.get(service.autoCompleteData).promise();

}
}
