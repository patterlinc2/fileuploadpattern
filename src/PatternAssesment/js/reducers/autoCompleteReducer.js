import * as CONST from '../constants/MVMConstants' 

const initilizeData = [{

  }]

const autoComplete = (state = initilizeData, action) => {
  if(action.type === CONST.AUTO_COMPLETE){
      return[
        ...state, {
            data:action.data,
            text: action.text
        }
      ]
  }else{
      return state
  }
}

export default autoComplete
