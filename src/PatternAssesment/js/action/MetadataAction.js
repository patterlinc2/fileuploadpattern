/*
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *.
 *
 * LinkMetaDataActions
 */
import MetaDataApi from '../api/MetadataApi'
import * as types from '../constants/MVMConstants'
import MetaDataService from '../../../common/util/metadataService';
import MDSConstants from '../constants/MDSConstants';
import MetadataUtils from '../util/MetadataUtils';

export function  fetchMetaData(metadata){
    return (dispatch, getState) => {
      if(metadata!==undefined && metadata.uuid!==undefined && metadata.uuid!==''){
      const bufferGet = MDSConstants.BUFFER_GET;
          bufferGet.data = {'uuid':metadata.uuid};
          bufferGet.libConfig = metadata.libConfig;
          //making service call
          const promise = MetaDataService.send(bufferGet);
          promise.then(function (replyGet) {
            //success handler
            //convert keyword from api to ui format
            if(replyGet && replyGet.keywords){
             replyGet.keywords = MetadataUtils.convert_to_ObjArray(replyGet.keywords);
              }
            metadata  = MetadataUtils.copy(replyGet, metadata); 
              dispatch({
                type: types.SET_UUID,
                QMD_Data : metadata
              })
            },function (error) {
                //failure handler
            dispatch({
                type: types.METADATA_SET_ERROR,
                QMD_Data: {'errMsg':error}
              });
          }).catch(e => {
            dispatch({
              type: types.METADATA_SET_ERROR,
              QMD_Data: {'errMsg':e.message}
            })
          });
      }else{
        dispatch({
          type: types.METADATA_GET,
          QMD_Data : metadata
        })
      } 
    }
}

export function fetchMetaDataTaxonomy(metadata){
    return dispatch => {
      MetaDataApi.get_QMD_Data().then( function (data){ 
        let taxonomyValue = JSON.parse(data.text);
        taxonomyValue = MetadataUtils.copy(metadata, taxonomyValue); 
                dispatch({
            type: types.SET_UUID,
            QMD_Data : taxonomyValue
        })
        dispatch(fetchMetaData(metadata));
      }, function (error) {
        dispatch({
          type: types.METADATA_SET_ERROR,
          QMD_Data: {'errMsg':error.message}
        })
      }).catch(e => {
        dispatch({
          type: types.METADATA_SET_ERROR,
          QMD_Data: {'errMsg':e.message}
        })
      });
  }
}

export function  saveMetaData(values){
    return (dispatch, getState) => {
    if(values){
    if(values.keywords){
          values.keywords = MetadataUtils.convert_to_StringArray(values.keywords)
    }
      values.timeRequired = MetadataUtils.getTimeObj(values);
      const state = getState().Metadatareducers;
      const buffer = MDSConstants.BUFFER;
      buffer.data = values;
      buffer.libConfig = state[0].libConfig;
      const promise = MetaDataService.send(buffer);
        promise.then(function (replyCreate) {
            values.uuid = replyCreate.uuid;
            values.urn = replyCreate.uuid;

          if(replyCreate.keywords){
              values.keywords = MetadataUtils.convert_to_ObjArray(replyCreate.keywords);
          }
            
            dispatch({
              type: types.SET_UUID,
              QMD_Data : values
            })
 },function (error) {
        dispatch({
          type: types.METADATA_SET_ERROR,
          QMD_Data: {'errMsg':error}
        })
      }).catch(e => {
        dispatch({
          type: types.METADATA_SET_ERROR,
          QMD_Data: {'errMsg':e.message}
        })
      });
    }
  }
}

export function populateAutoComplete(text) {
    return dispatch => {
      MetaDataApi.get_AutoComplete_Data(text).then( function (data){ 
        dispatch({
          type: types.AUTO_COMPLETE,
          data: JSON.parse(data.text),
          text
        });
        },function (error) {
        dispatch({
          type: types.METADATA_SET_ERROR,
          QMD_Data: {'errMsg':error.message}
        })
      }).catch(e => {
        dispatch({
          type: types.METADATA_SET_ERROR,
          QMD_Data: {'errMsg':e.message}
        })
      });
  }
}
