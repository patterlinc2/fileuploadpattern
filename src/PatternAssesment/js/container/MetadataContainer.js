/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
 import React from 'react';
 import Heading from '../../../common/components/Heading';
 import MVMComponent from '../components/MVM/MVM';
 import {fetchMetaDataTaxonomy, saveMetaData} from '../action/MetadataAction';
 import { connect } from 'react-redux'
 import * as types from '../constants/MVMConstants';


const getSelectedValues = (dataArray) => {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }
  return [];
}

const mapStateToProps = (state, ownProps) => {
    const metadata = getSelectedValues(state.Metadatareducers);
    let patConfig = {};
    if(ownProps && ownProps.patConfig && ownProps.patConfig.patSetup){
      patConfig = ownProps.patConfig;
    }

    let  product;
         if(state.autoComplete.length > 1){
        product = state.autoComplete[state.autoComplete.length-1].text;
     }
     else{
      product = metadata.product;
     }
     if (state.autoComplete.length > 1) {
       const metadata = state.form.mvm;
       const metadataReducer = getSelectedValues(state.Metadatareducers);
       return {
        uuid :(metadataReducer!==undefined  && metadataReducer.uuid!==undefined)?metadataReducer.uuid:'',
        filename : metadata.filename.value,
        name : metadata.name.value,
        urn : (metadataReducer!==undefined  && metadataReducer.urn!==undefined)?metadataReducer.urn:'',
        planId: metadata.planId.value,
        isbn: metadata.isbn.value,
        modNo: metadata.modNo.value,
        chapNo: metadata.chapNo.value,
        author: metadata.author.value,
        contentType : metadata.contentType.value,
        audience :  metadata.audience.value,
        difficultyLevel :metadata.difficultyLevel.value,
        knowledgeLevel : metadata.knowledgeLevel.value,
        alignType : metadata.alignType.value,
        discipline : metadata.discipline.value,
        goalAlignment : metadata.goalAlignment.value,
        timeReq : metadata.timeReq.value,
        desc : metadata.desc.value,
        keywords: metadata.keywords.value,
         product: product,
         hours: metadata.hours.value,
         mins: metadata.mins.value,
         secs: metadata.secs.value,
         copyrightInfo: metadata.copyrightInfo.value,
    }
     }
     if (metadata.contentTypeData && state.autoComplete.length === 1) {
      const initialValue = {
        uuid : metadata.uuid,
        filename : metadata.filename,
        name : metadata.name,
        urn : metadata.urn,
        planId: metadata.planId,
        isbn: metadata.isbn,
        modNo: metadata.modNo,
        chapNo: metadata.chapNo,
        author: metadata.author,
        contentType : metadata.contentType,
        audience :  metadata.audience,
        difficultyLevel :metadata.difficultyLevel,
        knowledgeLevel : metadata.knowledgeLevel,
        alignType : metadata.alignType,
        discipline : metadata.discipline,
        goalAlignment : metadata.goalAlignment,
        enabObj : metadata.enabObj,
        timeReq : metadata.timeReq,
        desc : metadata.desc,
        keywords: metadata.keywords,
         hours: metadata.hours,
         mins: metadata.mins,
         secs: metadata.secs,
          patConfig:patConfig,
          product: product,
         copyrightInfo: metadata.copyrightInfo,
          errMsg: metadata.errMsg,
      }
      return {
          suggestions: metadata.suggestions,
          contentTypeData : metadata.contentTypeData,
          audienceRolesData : metadata.audienceRolesData,
          difficultyLevelData : metadata.difficultyLevelData,
          knowledgeLevelData : metadata.knowledgeLevelData,
          alignmentTypeData : metadata.alignmentTypeData,
          disciplineData : metadata.disciplineData,
          goalAlignmentData : metadata.goalAlignmentData,
          languages : metadata.languages,
         errMsg: metadata.errMsg,
          uuid : metadata.uuid,
          filename : metadata.filename,
          name : metadata.name,
          urn : metadata.urn,
          planId: metadata.planId,
          isbn: metadata.isbn,
          modNo: metadata.modNo,
          chapNo: metadata.chapNo,
          author: metadata.author,
          contentType : metadata.contentType,
          audience :  metadata.audience,
          difficultyLevel :metadata.difficultyLevel,
          knowledgeLevel : metadata.knowledgeLevel,
          alignType : metadata.alignType,
          discipline : metadata.discipline,
          goalAlignment : metadata.goalAlignment,
          enabObj : metadata.enabObj,
          timeReq : metadata.timeReq,
          desc : metadata.desc,
          keywords: metadata.keywords,
          hours: metadata.hours,
         mins: metadata.mins,
         secs: metadata.secs,
          patConfig:patConfig,
          product: product,
           copyrightInfo: metadata.copyrightInfo,
          'initialValues': initialValue
    }
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  const metadata = {};
  if(ownProps && ownProps.patConfig && ownProps.patConfig.patSetup){
    if(ownProps.patConfig.patSetup.uuid){
      metadata.uuid = ownProps.patConfig.patSetup.uuid;
      metadata.urn = ownProps.patConfig.patSetup.uuid;
    }
    if(ownProps.patConfig.patSetup.filename){
      metadata.filename = ownProps.patConfig.patSetup.filename;
    }
    if(ownProps.patConfig.patSetup.planId){
      metadata.planId = ownProps.patConfig.patSetup.planId;
    }
  }

  if(ownProps && ownProps.libConfig){
    metadata.libConfig = ownProps.libConfig;
  }

  return {
    componentWillMount(){
      dispatch(fetchMetaDataTaxonomy(metadata))
    },
    onSave(values, dispatch){
      const data = values;
      dispatch(saveMetaData(data));
    },
    onSubmit(data){
      dispatch(saveMetaData(data));
    }
  }
}

const MetadataContainerConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true }
  )(MVMComponent)

  export default MetadataContainerConnect;
