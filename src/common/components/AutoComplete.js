import React from 'react'
import Autosuggest from 'react-autosuggest';
import _ from 'lodash';
//var _ = require('lodash');

function getSuggestions(value, languages) {
  if(value){
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;

  return inputLength === 0 ? [] : languages.filter(lang =>
    lang.name.toLowerCase().slice(0, inputLength) === inputValue
  );
}
}

function getSuggestionValue(suggestion) { // when suggestion selected, this function tells
  return suggestion.name;                 // what should be the value of the input
}

function renderSuggestion(suggestion) {
  return (
    <span>{suggestion.name}</span>
  );
}

class autoComplete extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value.value,
      suggestions: props.data
    };

    this.onChange = this.onChange.bind(this);
    this.onSuggestionsUpdateRequested = _.debounce(props.onSuggestionsUpdateRequested.bind(this),200);
  }

  onChange(event, { newValue }) {
    this.setState({
      value: newValue
    });
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.data.text){
      this.setState({
        suggestions: getSuggestions(nextProps.data.text, nextProps.data.data)
      });
    }else{
        suggestions: []
    }
  }

  render() {
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: 'Type a product',
      value,
      onChange: this.onChange
    };

    return (
      <Autosuggest suggestions={suggestions}
                   onSuggestionsUpdateRequested={this.onSuggestionsUpdateRequested}
                   getSuggestionValue={getSuggestionValue}
                   renderSuggestion={renderSuggestion}
                   inputProps={inputProps} {...this.state.product}/>
    );
  }
}

autoComplete.propTypes = {
  data: React.PropTypes.object,
  onSuggestionsUpdateRequested:React.PropTypes.func,
  value:React.PropTypes.string,

}

export default autoComplete;
