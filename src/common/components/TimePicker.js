import React from 'react';
class TimePicker extends React.Component {

	static propTypes = {
		id: React.PropTypes.string,
		name : React.PropTypes.string,	
		value : React.PropTypes.string,	
	}
	static defaultProps = {
		  id: '',
		  name:'',
		  value:''
  	}
    render(){
        return (
            <input type="time" id={this.props.id} name={this.props.name} value={this.props.value}{...this.props.value}  />
        )
    }
}

export default TimePicker
