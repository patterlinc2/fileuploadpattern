import React from 'react';
import NumericInput from 'react-numeric-input';

class TimeComponent extends React.Component {

  constructor(props){
        super(props);
        this.displayName = 'TimeComponent';
        this.state={
            hours:this.props.hours,
            mins :this.props.mins,
            secs:this.props.secs
        };
  }
  static propTypes= {
    id:React.PropTypes.string,
    hours: React.PropTypes.object,
    mins: React.PropTypes.object,
    secs: React.PropTypes.object    
  }
 static defaultProps ={
          id:'',
          hours:'',
          mins:'',
          secs:''
  }

	myFormat(num) {
		if(num >=10){
			return num;
		}else{
    		return 0 + num;
    	}
	}

  hoursOnchange(e){
    this.state.hours = this.myFormat(e);
  }

  minsOnchange(e){
    this.state.mins = this.myFormat(e);
  }

  secsOnchange(e){
    this.state.secs = this.myFormat(e);
  }


   render() {
      return (
         <div>
        
            <label>Hours</label>
            <NumericInput min={0} max={24} size={1} 
             format={this.myFormat} name="hh" onChange={this.hoursOnchange}
             value={this.state.hours} {...this.props.hours}/>&nbsp;&nbsp;
           
            
            <label>Minutes</label>
            <NumericInput min={0} max={60} size={1} 
             format={this.myFormat} name="mm" onChange={this.minsOnchange}
             value={this.state.mins} {...this.props.mins}/>&nbsp;&nbsp;
             
            
            <label>Seconds</label>
            <NumericInput min={0} max={60} size={1} 
             format={this.myFormat} name="ss" onChange={this.secsOnchange}
             value={this.state.secs} {...this.props.secs}/>&nbsp;&nbsp;
             
         </div>
      );
   }
}

export default TimeComponent;
