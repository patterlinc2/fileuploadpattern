import React from 'react';
import NumericInput from 'react-numeric-input';

class SecondsComponent extends React.Component {

constructor(props) {
    super(props);
    this.displayName = 'SecondsComponent';
     this.state={
            secs:this.props.secs
        };
 }

 static propTypes= {
    id:React.PropTypes.string,
    secs: React.PropTypes.object    
}
 static defaultProps ={
          id:'',
          secs:''
  }

	myFormat(num) {
		if(num >=10){
			return num;
		}else{
    		return 0 + num;
    	}
	}

 secsOnchange(e){
    this.state.secs = this.myFormat(e);
  }

   render() {
      return (
         <div className="pe-seconds">
            <label>Seconds</label><NumericInput min={0} max={60} size={1} 
            format={this.myFormat} name="ss" value={this.state.secs}{...this.props.secs}/>
         </div>
      );
   }
}
module.exports = SecondsComponent;
