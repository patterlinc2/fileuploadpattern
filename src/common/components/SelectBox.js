/*
 *
 *
 * 
 */
import React from 'react';


class SelectBox extends React.Component{
constructor(props) {
    super(props);
    this.displayName = 'SelectBox';
}
static propTypes= {
    id:React.PropTypes.string.isRequired,
    multiple: React.PropTypes.bool,
    disabled: React.PropTypes.bool,
    required: React.PropTypes.bool, 
    autofocus: React.PropTypes.bool,
    defaultValue:React.PropTypes.string,
    options:React.PropTypes.array.isRequired,
    handleChange:React.PropTypes.func,
    value:React.PropTypes.object
}
static defaultProps ={
      id:'',
      multiple: false,
      disabled:false,
      required:false,
      autofocus: false,
      defaultValue:'',
      options:[]
}
componentWillMount() {
        let defaultValue = this.props.value ;
        if(this.props.options!==undefined && this.props.options.length>0){
            this.props.options.map( function (CurrOption){ 
               if(defaultValue === CurrOption.text){
                    CurrOption.selected = 'selected';
                }else{
                    CurrOption.selected = '';
                }
            });
        }
}
componentDidMount() {
    if (this.props.autofocus){
      //this.refs.select.focus();
    }      
}

componentWillReceiveProps(){
    let defaultValue = this.props.value ;
        if(this.props.options!==undefined && this.props.options.length>0){
            this.props.options.map( function (CurrOption){ 
               if(defaultValue === CurrOption.text){
                    CurrOption.selected = 'selected';
                }else{
                    CurrOption.selected = '';
                }
            });
        }
}

render() {
        return (
            <select id={this.props.id} multiple={this.props.multiple} value ={this.props.value} {...this.props.value}  disabled={this.props.disabled} required={this.props.required}>
                {this.props.options.map((CurrOption, index) =>
                <option value = {CurrOption.text}  key={CurrOption.text} value={CurrOption.selected}>{CurrOption.text}</option>
            )}
            </select>
        )
    }

};

module.exports = SelectBox;
