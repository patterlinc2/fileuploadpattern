import React from 'react';
import ReactTags from 'react-tag-autocomplete';

class TagElem extends React.Component{
    constructor(props) {
        super(props);
        this.displayName = 'TagElem';
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddition = this.handleAddition.bind(this);

    }
    static propTypes= {
        tags:React.PropTypes.array,
        suggestions:React.PropTypes.array
    }
    static defaultProps ={
        tags:[],
        suggestions:[]
    }
    handleDelete(i) {
        let tags = this.props.tags;
        tags.splice(i, 1);
    }
   handleAddition(tag) {
        let tags = this.props.tags;
        tags.push({
            id: tags.length + 1,
            name: tag.name
        });
    }
    render() {

        return (
            <ReactTags 
                tags={this.props.tags}
                suggestions={this.props.suggestions}
                handleDelete={this.handleDelete}
                handleAddition={this.handleAddition}
                handleInputChange={this.handleInputChange} 
                placeholder = ""/>
        )
    }
};
module.exports = TagElem;
