/**
 * @module PatternsLib
 */

import React from 'react';
import {render, unmountComponentAtNode} from 'react-dom';
import Assesment from './PatternAssesment/js/components/App';
import Question from './PatternQuestion/js/components/App';
import AddAnAsset from './PatternAddAnAsset/js/components/App';
import bean from 'bean';

let libConfig = {};
let token = 1;

const typeList = {
                   ASSESMENT:'Assesment',
                   QUESTION:'Question',
                   AddAnAsset: 'AddAnAsset'
                 };
const compList = {};
compList[typeList.ASSESMENT] = Assesment;
compList[typeList.QUESTION] = Question;
compList[typeList.AddAnAsset] = AddAnAsset;

/**
 * Create a component
 * This is a private function.
 * @function createComp
 * @param {string} pattern - a pattern component name
 * @param {object} patConfig - a configuration for pattern component
 * @return {object} component - a react component
 */
function _createComp(pattern, patConfig) {
    const component = compList[pattern];
    return React.createElement(component, {
        libConfig: libConfig,
        patConfig: patConfig
    });
};

/**
 * Factory to create an instance of pattern component.
 * This is a private function.
 * @param {string} pattern - a pattern component name
 * @returns {object} and instance of pattern component
 */
function _factory(pattern) {

    let resultsCB;
    const instance = {
        patSetup : null,                                      // user supplied setup config
        pattern : pattern,                                    // pattern name
        uqid : token,                                         // unique id to distinguish each instance
        resultsEventId : pattern + '-' + token,               // unique results event id for receiving response
        eventId : pattern + '-channel-' + token,              // unique event channel for communicating with instance
        component : null,
        renderedComponent:null,
        /**
         * Setup pattern component using supplied configuration options,
         * and register for an event which will return results
         *
         * @function setup
         * @param {object} patSetup - a config object for the pattern setup
         * @param {function cb - a callback function which will receive the results
         */
        setup : function (patSetup, cb) {

            resultsCB = cb;
            this.patSetup = Object.assign(patSetup);
            token++;
		        bean.on(this,
                    this.resultsEventId,
                    function (data) {
			                  if (resultsCB) {
                            resultsCB(data);
                        }

		                });
        },
        /**
         * Run the component.
         * The component may render a GUI at prespecified selector
         * or it may just be used to offload some computing.
         * @function run
         */
        run : function () {

            this.component = _createComp(pattern, this);
		    this.renderedComponent = render(this.component,
                   document.querySelector(this.patSetup.selector)
                  );
        },
        on : function (cb) {

            bean.on(this, this.eventId, cb);
        },
        off : function () {

            bean.off(this, this.eventId);
        },
        fire : function (...msgs) {
            let msgFromComp = msgs;
            if(this.renderedComponent.refs.mvmContainer){
               msgFromComp = this.renderedComponent.refs.mvmContainer.context.store.getState().form.mvm.uuid;
               this.renderedComponent.refs.mvmContainer.getWrappedInstance().getWrappedInstance().submit();
            }
            bean.fire(this, this.eventId, msgFromComp);
        },
        unmount : function () {

            unmountComponentAtNode(document.querySelector(this.patSetup.selector));
        }
    };
    return instance;
};
export default {
    type : typeList,
    setup : function (config) {
        libConfig = config;
    },
    create : function (pattern) {
        return _factory(pattern);
    }
}
