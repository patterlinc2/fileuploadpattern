import { METADATA_GET, SAVE_METADATA } from '../constants/MVMConstants'
import Immutable from 'immutable'

const initilizeValues = [{
  uuid : '' , 
  filename:'',
  name:'',
  urn:'',
  enabObj:'' ,
  planId: '',
  modNo:'',
  chapNo:'',
  author:'',
  hours: '',
  mins: '',
  secs: '',
  copyRight: '',
  contentTypeData: [],
  audienceRolesData: [],
  difficultyLevelData: [],
  knowledgeLevelData: [],
  alignmentTypeData: [],
  disciplineData: [],
  goalAlignmentData: [],
  languages: [],
  timeReq:'' ,
  isbn: '',
  desc:'',
  keywords: [],
  product:'',
  errMsg:''
}]

const Metadatareducers = (state = initilizeValues, action)=>{
  switch(action.type) {
    case 'METADATA_SET_ERROR':
    case 'METADATA_GET':
    if(state!==undefined && state[0]!==undefined){
      return[
         Immutable.fromJS(state[0]).merge(Immutable.Map(action.QMD_Data)).toJS()
       ]
    }else{
      return[
        Immutable.fromJS(initilizeValues).merge(Immutable.Map(action.QMD_Data)).toJS()
      ]
    }
    case 'SAVE_METADATA':
    if(state!==undefined && state[0]!==undefined){
      return[
        Immutable.fromJS(state[0]).merge(Immutable.Map(action.values)).toJS()
      ]
    }else{
      return[
        Immutable.fromJS(initilizeValues).merge(Immutable.Map(action.values)).toJS()
      ]
    }
    case 'SET_UUID':
      return[
        Immutable.fromJS(state[0]).merge(action.QMD_Data).toJS()
      ]
    default:
    return state
    }
  }
  export default Metadatareducers;

  
  
