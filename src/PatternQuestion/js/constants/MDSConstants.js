const buffer = {
            'req'       : 'QuestionMetadata',             // ProductMetadata / AssessmentMetadata / QuestionMetadata / AssetMetadata
            'action'    : 'Create',                      // Create / Read One / Update / Delete / Search / Read All
}
 
const bufferGet = {
            'req'       : 'QuestionMetadata',             // ProductMetadata / AssessmentMetadata / QuestionMetadata / AssetMetadata
            'action'    : 'Read One',                      // Create / Read One / Update / Delete / Search / Read All
}

const MDSConstants = {
	BUFFER : buffer,
	BUFFER_GET : bufferGet
}

module.exports = MDSConstants;
