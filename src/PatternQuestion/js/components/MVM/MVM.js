/**
 * Copyright
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

/**
 * This component operates as a "Controller-View".  It listens for changes in
 * the TodoStore and passes the new data to its children.
 */
import React, {Component, PropTypes} from 'react';
import Label from'../../../../common/components/Label';
import TextBox from '../../../../common/components/TextBox';
import Heading from '../../../../common/components/Heading';
import SelectBox from '../../../../common/components/SelectBox';
import TextArea from '../../../../common/components/TextArea';
import TimeComponent from '../../../../common/components/TimeComponent';
import HoursComponent from '../../../../common/components/HoursComponent';
import MinutesComponent from '../../../../common/components/MinutesComponent';
import SecondsComponent from '../../../../common/components/SecondsComponent';
import TagElem from '../../../../common/components/TagElem';
import TimePicker from'../../../../common/components/TimePicker';
import {reduxForm} from 'redux-form';
import FormMessages from 'redux-form-validation';
import {generateValidation} from 'redux-form-validation';
import Autosuggest from '../../container/autoCompleteContainer';
import {injectIntl, intlShape} from 'react-intl';
import {messages} from './MVMDefaultMessages';
import bean from 'bean';
import {fetchMetaData, saveMetaData} from '../../action/MetadataAction';
export const fields = ['uuid','filename', 'name', 'urn', 'isbn','modNo','chapNo','author','contentType', 
                       'audience','difficultyLevel','knowledgeLevel','alignType','product', 'discipline', 
                       'planId','goalAlignment','enableObj','timeReq','desc','keywords','hours','mins','secs','copyRight'];
/*const languages = [
  {
    name: 'C',
    year: 1972
  },
  {
    name: 'Elm',
    year: 2012
  },
  {
    name:'welcome',
    year: 2013
  }
]*/
 
/*function getSuggestions(value) {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
 
  return inputLength === 0 ? [] : languages.filter(lang =>
    lang.name.toLowerCase().slice(0, inputLength) === inputValue
  );
}
 
function getSuggestionValue(suggestion) { // when suggestion selected, this function tells 
  return suggestion.name;                 // what should be the value of the input 
}
 
function renderSuggestion(suggestion) {
  return (
    <span>{suggestion.name}</span>
  );
}
*/
let validations = {
     name: {
       required: true
     },
     /*filename: {
       required: true
     },*/
     'filename': false,
     'uuid': false,
     'urn': false,
     'contentType': false,
     'audience': false,
     'difficultyLevel': false,
     'knowledgeLevel': false,
     'discipline': false,
     'alignType': false,
     'enableObj': false,
     'timeReq': false,
     'goalAlignment': false,
     'desc': false,
     'isbn' : false,
     'modNo' : false,
     'planId': false,
     'chapNo': false,
     'author': false,
     'keywords': false,
     'product':false,
     'hours':false,
     'mins':false,
     'secs':false,
     'copyRight': false
};

class MVMComponent extends React.Component{
  static PropTypes = {
        intl: intlShape.isRequired
    }

constructor(props) {
    super(props);
    this.displayName = 'MVMComponent';
    this.componentWillMount = props.componentWillMount;
    this.onSave = props.onSave;
    this.onChange = this.onChange.bind(this);
    this.onSuggestionsUpdateRequested = this.onSuggestionsUpdateRequested.bind(this);

     this.state = {
      contentTypeData: [],
      audienceRolesData: [],
      difficultyLevelData: [],
      knowledgeLevelData: [],
      alignmentTypeData: [],
      disciplineData: [],
      goalAlignmentData: []  ,
      languages: [],
      value: '',
      suggestions: this.getSuggestions('')   
    }
}

componentWillReceiveProps(nextProps) {
bean.fire(this.props.patConfig, this.props.patConfig.resultsEventId,nextProps);

    if (nextProps.contentTypeData) {
      this.state.contentTypeData = nextProps.contentTypeData;
    }
    if (nextProps.audienceRolesData) {
      this.state.audienceRolesData = nextProps.audienceRolesData;
    }
     if (nextProps.difficultyLevelData) {
      this.state.difficultyLevelData = nextProps.difficultyLevelData;
    }
    if (nextProps.knowledgeLevelData) {
      this.state.knowledgeLevelData = nextProps.knowledgeLevelData;
    }   
    if (nextProps.alignmentTypeData) {
      this.state.alignmentTypeData = nextProps.alignmentTypeData;
    }
    if(nextProps.disciplineData){
      this.state.disciplineData = nextProps.disciplineData;
    }
    if(nextProps.goalAlignmentData){
      this.state.goalAlignmentData = nextProps.goalAlignmentData;
    }
    if(nextProps.languages){
      this.state.languages = nextProps.languages;
    }
}
 
onBlur(e) {
  return true;
}

onChange(e) {
  return true;
}

_onChange(e){
        let val = e.target.value,
            name = e.target.name
        for(let key in this.state.fields){
            let field = this.state.fields[key]
            if(field.name == name){
                field.value = val
            }
        }
        this.setState(this.state)
    }

onChange(event, { newValue }) {
    this.setState({
      value: newValue
    });
  }
 
onSuggestionsUpdateRequested({ value }) {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  }

 getSuggestions(value) {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
 
  return inputLength === 0 ? [] : this.state.languages.filter(lang =>
    lang.name.toLowerCase().slice(0, inputLength) === inputValue
  );
}
 
 getSuggestionValue(suggestion) { // when suggestion selected, this function tells 
  return suggestion.name;                 // what should be the value of the input 
}
 
 renderSuggestion(suggestion) {
  return (
    <span>{suggestion.name}</span>
  );
}


render() {
  const {formatMessage} = this.props.intl;
  let self = this
  //const languagesData = this.state.languages || [];
  const contentTypeData = this.state.contentTypeData || [];
  const audienceRolesData = this.state.audienceRolesData || [];
  const difficultyLevelData = this.state.difficultyLevelData || [];
  const knowledgeLevelData = this.state.knowledgeLevelData || [];
  const alignmentTypeData = this.state.alignmentTypeData || [];
  const disciplineData = this.state.disciplineData || [];
  const goalAlignmentData = this.state.goalAlignmentData || [];
  const { value, suggestions } = this.state;
  const inputProps = {
      placeholder: 'Type a product',
      value,
      onChange: this.onChange
  };
  const{
    fields : {uuid, filename,name,urn,contentType,audience,modNo,
              author,planId,chapNo,difficultyLevel,knowledgeLevel,discipline,
              alignType,isbn,goalAlignment,enableObj,timeReq,desc,keywords,
              product,hours,mins,secs,copyRight},handleSubmit
        }= this.props;

    return (
            <form>
            <div className="pe-updatemetadata">
            <section>
              <h2>{formatMessage(messages.MVM_Data)}</h2>
              <div className="pe-input pe-input--horizontal navcontainer">
                    <Label for ="name" text={formatMessage(messages.MVM_Name)}/>
                    <TextBox value = {name} required={Boolean(true)} placeholder = "Add a descriptive title"/>
                    <FormMessages tagName="ul" field={name}>
                      <li when="required">
                        Error: Required Field
                      </li>
                    </FormMessages>
                </div>
                <div className="pe-input pe-input--horizontal" >
                    <Label for ="Product" text={formatMessage(messages.MVM_Product)}/>
                       <Autosuggest id="product" {...product} value ={product}/>
                </div>
                <div className="pe-input pe-input--horizontal" >
                    <Label for="UUID" text={formatMessage(messages.MVM_UUID)}/>
                    <TextBox value={uuid} disabled={Boolean(true)} required={Boolean(true)}/>
                </div>
                < div className="pe-input pe-input--horizontal" >
                    <Label for ="AssignmentTitle" text={formatMessage(messages.File_Name)}/>
                    <TextBox value={filename} disabled={Boolean(true)}/>
                </div>
                 <div className="pe-input pe-input--horizontal">
                    <Label for="Description" text={formatMessage(messages.MVM_Desc)}/>
                    <TextArea id="description" value={desc} placeholder = "Summary description of the resource"/>
                </div>
                <div className="pe-input pe-input--horizontal" >
                    <Label for ="URI" text={formatMessage(messages.MVM_URN)}/>
                    <TextBox value = {urn} disabled={Boolean(true)} placeholder = "System entered canonical URI for asset"/>
                </div>
                <div className="pe-input pe-input--horizontal">
                    <Label for ="ContentType" text={formatMessage(messages.Content_Type)}/>
                    <SelectBox id="contentType" value={contentType} 
                     onChange= {this.onChange}   onBlur= {this.onBlur} 
                     options={contentTypeData}/>                         
                </div>
                <div className="pe-input pe-input--horizontal" >
                  <Label for ="keywords" text={formatMessage(messages.MVM_Keywords)}/>
                  <TagElem suggestions={this.props.suggestions} tags={keywords.value}/>
                </div>
                 <div className="pe-input pe-input--horizontal pe-timer">
                <Label for="timeReq" text={formatMessage(messages.Time_Required)}/>               
                  <HoursComponent id="hours" hours={hours}/>              
                  <MinutesComponent id="minutes" mins={mins}/>
                  <SecondsComponent id="secs" secs={secs}/>
                </div>
                 <div className="pe-input pe-input--horizontal">
                    <Label for ="Alignment Type" text={formatMessage(messages.Publisher)}/>
                    <TextBox value={alignType} placeholder="Add publisher"/>
                </div>
                 <div className="pe-input pe-input--horizontal">
                    <Label for ="Discipline" text={formatMessage(messages.Discipline)}/>
                    <SelectBox id="discipline"  value={discipline} 
                     onChange= {this.onChange}   onBlur= {this.onBlur} options={disciplineData}/>
                </div>
                <div className="pe-input pe-input--horizontal">
                    <Label for="EnablingObejctive" text={formatMessage(messages.Objective_Alignment)}/>
                    <TextBox value = {enableObj} placeholder="Add in Learning Objective URI"/>
                </div>              
                <div className="pe-input pe-input--horizontal">
                    <Label for ="Goal Alignment" text={formatMessage(messages.Goal_Alignment)}/>
                    <SelectBox id="goalAlignment" value={goalAlignment} 
                     onChange= {this.onChange}   onBlur= {this.onBlur} 
                     options={goalAlignmentData}/>                       
                </div>        
                <div className="pe-input pe-input--horizontal">
                    <Label for ="Difficulty Level" text={formatMessage(messages.Difficult_Level)}/>
                    <SelectBox id="difficultLevel" value={difficultyLevel} 
                     onChange= {this.onChange}   onBlur= {this.onBlur} options={difficultyLevelData}/>
                </div>
                <div className="pe-input pe-input--horizontal">
                    <Label for ="Audience Role" text={formatMessage(messages.Audience_Role)}/>
                    <SelectBox id="audienceRole" value={audience} 
                     onChange= {this.onChange}   onBlur= {this.onBlur} 
                     options={audienceRolesData}/>                       
                </div>
                 <div className="pe-input pe-input--horizontal" >
                    <Label for ="PlanId" text={formatMessage(messages.MVM_PLAN_ID)}/>
                    <TextBox value = {planId} disabled={Boolean(true)}/>
                </div>
                 <div className="pe-input pe-input--horizontal" >
                    <Label for ="ISBN" text={formatMessage(messages.MVM_ISBN)}/>
                    <TextBox value = {isbn} placeholder="Add an ISBN"/>
                </div>
                <div className="pe-input pe-input--horizontal" >
                    <Label for ="ModNo" text={formatMessage(messages.MVM_Module_No)}/>
                    <TextBox value = {modNo} placeholder="Add an Module Number"/>
                </div>
                 <div className="pe-input pe-input--horizontal" >
                    <Label for ="chapNo" text={formatMessage(messages.MVM_Chapter_No)}/>
                    <TextBox value = {chapNo} placeholder="Add an Chapter Number"/>
                </div>
                 <div className="pe-input pe-input--horizontal" >
                    <Label for ="author" text={formatMessage(messages.MVM_Author)}/>
                    <TextBox value = {author} placeholder="Add the author name"/>
                </div>
                 <div className="pe-input pe-input--horizontal" >
                    <Label for ="copyRight" text={formatMessage(messages.MVM_COPY_RIGHT)}/>
                    <TextBox value = {copyRight} placeholder="Add Copyright information"/>
                </div>
            </section>
            </div>
            </ form>
        )
    }
};

MVMComponent.propTypes = {
        intl: intlShape.isRequired,
        componentWillMount:React.PropTypes.func,
        onSave:React.PropTypes.func,
        contentType: React.PropTypes.string,
        suggestions: React.PropTypes.string,
        alignType: React.PropTypes.string,
        discipline: React.PropTypes.string,
        goalAlignment: React.PropTypes.string,
        difficultyLevel: React.PropTypes.string,
        audience: React.PropTypes.string,
        knowledgeLevel: React.PropTypes.string,
        fields: React.PropTypes.object,
        handleSubmit: React.PropTypes.func,
        patConfig:React.PropTypes.object,
        onSubmit: React.PropTypes.func
}

MVMComponent = reduxForm({
    form: 'mvm',
     fields: ['uuid', 'filename','name','urn','isbn','planId','modNo','chapNo','author',
              'contentType','audience','difficultyLevel','knowledgeLevel','alignType',
              'discipline','goalAlignment','enableObj','timeReq','desc','keywords',
              'product','hours','mins','secs','copyRight'],
     ...generateValidation(validations)
  })(MVMComponent);

//module.exports = MVMComponent;
export default injectIntl(MVMComponent, {withRef: true});
