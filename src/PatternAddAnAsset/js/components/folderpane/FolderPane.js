import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Image from '../common/Image';
import TreeNodeUtil from './TreeNodeUtil';
import {getNodeRef, trimFolderName, highlightChildren} from '../common/browseAssetUtil';

class FolderPane extends Component{

	constructor(props){
        super(props);
        this.state={
            expanded : false,
            style:''
        }
        if(this.props.componentWillMount){
            this.componentWillMount = props.componentWillMount.bind(this);
            // this.componentDidUpdate = props.componentDidUpdate.bind(this);
        }
	        if(this.props.toggle){
	        	this.toggle = this.toggle.bind(this);
	        }
    }

   	toggle(model, foldername, nodeRef) {
    		let child;
    		let oldStyle;
    		this.setState({expanded: !this.state.expanded});
    		for(let obj in model){
    			child = model[obj];
    			if(child.fileName === foldername){
    				if(!child.highlight){
	    				oldStyle = child.style;
	    				if(!oldStyle.includes('tree-node-selected')){
	    					oldStyle = oldStyle + ' '+ 'tree-node-selected'
	    				}
    					child.style = 'pe_filter_enabled'+' '+oldStyle;
    					child.items = TreeNodeUtil.visibleChildren(child.items);
    					child.highlight = true;
    				}else{
    					child.style = child.style.replace('pe_filter_enabled', '');
    					child.items = TreeNodeUtil.hideChildren(child.items);
    					child.highlight = false;
    				}
    			}else{
    				if((child.style.includes('pe_filter_enabled') || 
    					child.style.includes('tree-node-selected'))){
	    				child.style = child.style.replace('pe_filter_enabled', '');
	    				child.style = child.style.replace('tree-node-selected', '');
	    				child.items = TreeNodeUtil.hideChildren(child.items);
	    				child.highlight = false;
    			}

    			}
    		}
        	this.props.toggle(model,foldername, nodeRef);
    	}

		render(){
			let self = this;
			let model = this.props.model;
			let folderArr = [];
			let items;
			let childrens = [];
			let holder = [];
			let nodeRef;
        	let folderName;
        	let fileNameObj;
        	let isParent;
        	let cssStyle;
			for(let folder in model){
				folderName = trimFolderName(model[folder].fileName);
				fileNameObj = model[folder];
					if(fileNameObj.fileName){
						if(fileNameObj.expanded){
							nodeRef = getNodeRef(fileNameObj.nodeRef);
							cssStyle = fileNameObj.style;
							folderArr.push(
								<div className='pe_filter no-margin'
								key={fileNameObj.fileName}
								onClick={this.toggle.bind(self,model, fileNameObj.fileName, nodeRef)}> 
								<i className="fa fa-folder-open folderIcon"></i>
								<span id={nodeRef} name={fileNameObj.fileName} key={fileNameObj.fileName} className={cssStyle}>{folderName}</span>
								</div>
							);
							if(fileNameObj.items){
								items = fileNameObj.items;
								folderArr.push(<div key={Math.random()} className='pe_filter'>
												<FolderPane 
												model={items} 
												toggle={self.props.toggle}/>
												</div>);
							}

						}
			}
		}
			return(
				<div>
					{ folderArr }
				</div>
				);
		}
}

FolderPane.propTypes = {
    componentWillMount: PropTypes.func,
    toggle: PropTypes.func,
    model: PropTypes.array
}

export default FolderPane;
