import {getFirstObj} from '../common/browseAssetUtil';
export default{

	getCustomFolder(jsonData){
		let treeFolder = this.treeNodeUtil(jsonData);
		let sortedFolder = this.sortFolderTree(treeFolder);
		let treeContainer = this.expandFirstChild(sortedFolder);
		return treeContainer;
	},

	treeNodeUtil(jsonData){
		let treeContainer = [];
		let tempObj,
			baseTypeId,
			objectId;
		for(let obj in jsonData){
			tempObj = jsonData[obj];
			baseTypeId = tempObj.object.object.properties['cmis:baseTypeId'].value;
			objectId = tempObj.object.object.properties['cmis:objectTypeId'].value
				if(baseTypeId === 'cmis:folder'){
					if(objectId != 'F:fm:topic'){
						let treeObj = {};
						treeObj.path = tempObj.object.object.properties['cmis:path'].value;
						treeObj.fileName = tempObj.object.object.properties['cmis:name'].value;
						treeObj.nodeRef = tempObj.object.object.properties['alfcmis:nodeRef'].value;
						treeObj.highlight = false;
						treeObj.uniqueId = false;
						treeObj = this.isTreeExpanded(treeObj, obj);
						if(tempObj.children){
							treeObj.items = this.treeNodeUtil(tempObj.children);
							if(treeObj.items != undefined){
								treeObj.style = treeObj.items.length > 0 ? ' link-view' : 'pe_filter no-margin';
						    }
						}else{
							treeObj.style = 'pe_filter no-margin';
						}
						treeContainer.push(treeObj);
					}
				}
		}
		return treeContainer;
	},

	expandFirstChild(treeContainer){
		let child;
		for(let obj in treeContainer){
			child = treeContainer[obj];
			if(obj == 0){
				child.highlight = true;
				child.style = 'pe_filter_enabled tree-node-selected';
				if(child.items){
					if(child.items.length > 0){
						child.style = child.style+' link-view';
						child.items = this.visibleChildren(child.items);
					}
				}
				treeContainer[obj] = child;
				break;
			}
		}

		return treeContainer;
	},

	isTreeExpanded(treeObj, index){
		let isParent = this.pathOccurance(treeObj.path);
			if(isParent === 1){
				treeObj.expanded = true;
				// console.log("index :"+index+" filename :"+treeObj.fileName);
				// this.expandFirstChild(treeObj,index);
			}else{
				treeObj.expanded = false;
			}
		return treeObj;
	},

	sortFolderTree(treeObj){
		let sortedObjs;
		if(treeObj != undefined){
			if(treeObj.length > 0){
			sortedObjs = _.sortBy(treeObj,'fileName');
			}
		}
		return sortedObjs;
	},

	pathOccurance(cmisPath){
		let isParent;
		if(cmisPath != undefined){
			let startIndex = cmisPath.indexOf('/');
			if(startIndex === 0){
				let parent = cmisPath.split('/');
				isParent = parent.length-1;
			}else{
				console.log('Path is not started with /');
			}
		}
		return isParent;
	},

	visibleChildren(childObj){
		let child;
		childObj = this.sortFolderTree(childObj);
		for(let obj in childObj){
    			child = childObj[obj];
    			child.expanded = !child.expanded;
    			child = this.disableEnabledNode(child);
    			childObj[obj] = child;
    		}
    		return childObj;
	},

	hideChildren(childObj){
		let child;
		childObj = this.sortFolderTree(childObj);
		for(let obj in childObj){
    			child = childObj[obj];
    			child.expanded = false;
    			child = this.disableEnabledNode(child);
    			if(child.items){
    				this.hideChildren(child.items);
    			}
    			childObj[obj] = child;
    		}
    		return childObj;
	},

	disableEnabledNode(child){

		if((child.style.includes('pe_filter_enabled') || 
    		child.style.includes('tree-node-selected'))){
	    		child.style = child.style.replace('pe_filter_enabled', '');
	    		child.style = child.style.replace('tree-node-selected', '');
	    		child.highlight = false;
    	}
    	return child;

	}
}
