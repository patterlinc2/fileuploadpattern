import React, { Component, PropTypes } from 'react';

class RenderColumn extends Component{

render(){

	const column = this.props.cols.map(function (currColumn,index){
		return <div className='col-3' key={index}><b>{currColumn}</b></div>;
	});

	return(
		<div className='row'>
			{column}
		</div>
		)
}
}

RenderColumn.propTypes = {
    cols: PropTypes.array
}

export default RenderColumn;
