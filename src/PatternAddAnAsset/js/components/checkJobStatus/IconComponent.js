import React, { Component, PropTypes } from 'react';

class IconComponent extends Component{



render(){
	const img = this.props.src;
	return(
			<div>
				<img alt="alt text" src={img}/>
			</div>

		);
}
}

IconComponent.propTypes = {
src : PropTypes.string
}
export default IconComponent;
