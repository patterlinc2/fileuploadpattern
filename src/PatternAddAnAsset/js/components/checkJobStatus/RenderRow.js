import React, { Component, PropTypes } from 'react';
import Image from './IconComponent.js';
import  ProgressBar from '../common/ProgressBar';
let img = '/images/accept.png';
let loadimg = '/images/loader.gif';


class RenderRow extends Component{

constructor(props){
    super(props);
    this.state={
      visible:true,
      display : ''
    };
    this.toggle = this.toggle.bind(this);
}

toggle(type){
	const newState = !this.state.visible;
	const name = type.Name;
    this.setState({visible: newState});
    this.setState({display: name});
    this.props.parent(newState, name);
}


getAssets(node, style){
    const assets = node;
    let assetArry = [];
    let key;
      	for(key in assets){
     		 assetArry.push(
     		 			<div key={key}className="children">
     		 			<div className="col-13" style={style}>{assets[key].name}</div>
      					<div className="col-13" style={style}>{assets[key].size}</div>
      					<div className="col-13" style={style}><ProgressBar percentage={'0'}/></div>
      					<div className="col-13" style={style}>

      					<span className="img"><Image src={img}/></span>
      					<span>{assets[key].status}</span>
      					</div>


      					</div>
                    );
    }
    return assetArry;
  }

getChildren(self, assetRow){

	let asset = assetRow.assets;
 	let style = {};
	if(assetRow.Name !== this.state.display){
      style.display = 'none';
    }else if(assetRow.Name === this.state.display){
    	if(this.state.visible){
    		style.display = 'none';
    	}else{
    		style.display = '';
    	}
    }
	return this.getAssets(asset, style);
}


render(){
	let childList;
	let self = this;
	let nameElement;
	let test = this.props.myFunc;
	let rows = this.props.rows.map(function (item,index){
	let rowArr = [];
	childList = self.getChildren(self,item);

		for(let row in item){

			if(row == 'Name'){
				//nameElement = <u><a onClick={self.toggle.bind(self,item)}>{item[row]}</a></u>;
				nameElement = <u><a className="fileName">{item[row]}</a></u>;

			}
			else if(row == 'Progress'){
				//console.log("Progress : "+item[row]);
				if(item[row] === 100){
					nameElement = <ProgressBar percentage={'100'}/>;

				}else{
					nameElement = <ProgressBar percentage={item[row]}/>;
				}
			}else if(row == 'status'){
				//console.log("Status : "+item[row]);

				if(item[row] === 'Success') {
				nameElement = <div><span className='parentImg'><Image src={img}/></span><span>{item[row]}</span></div>
				}else if(item[row] === 'Uploading'){
				nameElement = <div><span className='parentImg'><Image src={loadimg}/></span><span>{item[row]}</span></div>
				}else{
			     //console.log("Status : "+item[row]);

			     nameElement = <div><span><i className="fa fa-times-circle"></i>{item[row]}</span></div>
				}
			}else{
				if(row != 'assets'){
					nameElement = item[row];
				}else{
					nameElement = '';
				}
			}

			if(nameElement != ''){
				rowArr.push(<div key={Math.random()} className="col-3">{nameElement}</div>);
			}
		}
		rowArr.push(childList);
		return <div key={Math.random()} className="sub-rows">{rowArr}</div>

	});

	return(
			<div>
				{rows}
			</div>
		)
}
}
RenderRow.propTypes={
	parent:PropTypes.func,
    myFunc:PropTypes.func,
    rows: React.PropTypes.oneOfType([
      React.PropTypes.object,
      React.PropTypes.array,
    ])
};
export default RenderRow;
