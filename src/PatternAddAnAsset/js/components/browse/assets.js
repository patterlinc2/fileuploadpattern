import React, { Component, PropTypes } from 'react';
import Radio from '../common/Radio';
import PE_tooltip from '../common/pe-tooltip';
import {injectIntl} from 'react-intl';
import {getModifiedOn} from '../common/browseAssetUtil';
import ReactDOM from 'react-dom';

class assets extends Component {
  static propTypes = {
      intl: PropTypes.object,
      assetSelectedEvent: PropTypes.func,
      record: PropTypes.object,
      productTemp: PropTypes.object,
      setSelectedItem: PropTypes.func,
      selectedRecord: PropTypes.object,
      pageView: PropTypes.string
  }

  assetSelectedEvent(me, isTrue=true) {
    if (isTrue) {
      this.customFn(this.props.record);
    }
    else {
      this.customFn({});
    }
  }

  changeRadioButton(event) {
    if (event.target.type !== 'radio') {
      let radioComponent = ReactDOM.findDOMNode(this.refs.radioComp);

      radioComponent.checked = !radioComponent.checked;
      this.refs.radioComp.assetSelectedEvent('', radioComponent.checked);
    }
  }

  render() {
    const {formatDate} = this.props.intl;

     let item = this.props.productTemp,
        selectedRecord = this.props.selectedRecord,
        checked = false, fileSize, fileType = 'KB',
        setSelectedItem = this.props.setSelectedItem;

        fileSize = parseFloat(item.size/1024).toFixed(2);
        if (fileSize >= 1024) {
          fileSize = parseFloat(fileSize/1024).toFixed(2);
          fileType = 'MB'
        }
        /*
          Once we receive icons from alfresco server we will remove this.
          Also if we get  thumbnail images for audio and video.
        */
       if(item.mimetype.includes('image')==true){
        item.IconClass = 'fa-picture-o';
        item.url = item.url + '&c=queue&ph=true';
       }
       else if(item.type == 'wikipage'){
         item.url = 'http://10.219.43.16:8080/share/res/js/aikau/1.0.8.1/alfresco/search/css/images/topic-post.png';
       }
       else if(item.type == 'forumpost'){
         item.url = 'http://10.219.43.16:8080/share/res/js/aikau/1.0.8.1/alfresco/search/css/images/topic-post.png';
       }
       else if(item.type == 'link'){
         item.url = 'http://10.219.43.16:8080/share/res/js/aikau/1.0.8.1/alfresco/search/css/images/link.png';
       }
       else if(item.type == 'datalistitem'){
         item.url = 'http://10.219.43.16:8080/share/res/js/aikau/1.0.8.1/alfresco/search/css/images/datalistitem.png';
       }

       else if(item.mimetype.includes('video')==true){
         item.url = item.url + '&c=queue&ph=true';
         item.IconClass = 'fa-video-camera';
       }else if(item.mimetype.includes('audio')==true){
         item.IconClass = 'fa-volume-down';
         item.url = item.url + '&c=queue&ph=true';
       }
       else if(item.mimetype.includes('')==true){
         item.url = item.url + '&c=queue&ph=true';
       }

       else{
        item.IconClass = 'doc';

       }

       if (selectedRecord && selectedRecord.nodeRef === item.nodeRef) {
          checked = true;
       }
       let fileName;
       if(item.title){
        fileName = item.title;
       }else{
       let splitName = item.name.split('.');
       fileName = splitName[0];
      }
       let pageRender;
       let radioBtn = <Radio name='assetsCheckbox' ref='radioComp' record={item} checked= {checked} customFn = {setSelectedItem} parent = {this.assetSelectedEvent}/>
       let imgTag = <img src={item.url} className='card-img' alt='product image' />
       let self = this;
       if(this.props.pageView === 'grid-view'){
              pageRender = function (){ return (<div onClick={self.changeRadioButton.bind(self)} key={item.nodeRef} className='card-item1 search-result-box'>
              {radioBtn}
              <div className='thumbnail card-body'>
                {imgTag}
              </div>
              <footer className=''>
                <PE_tooltip position='right' content={fileName}>
                <label><a className='ellipsis_inline title'><strong>{fileName}</strong></a></label>
                </PE_tooltip>
                <div className='footer-icon'>
                    <i className= {'browse-tooltip fa ' + item.IconClass}></i>
                      <PE_tooltip position='right' content={'Uploaded by: '+item.modifiedBy+' Date uploaded: '+formatDate(item.modifiedOn)+' File size: '+ fileSize + fileType}>
                        <i className='fa fa-info-circle'></i>
                      </PE_tooltip>
                </div>
              </footer>
              </div>)
            }
         }else{

              pageRender = function (){ return (<div onClick={self.changeRadioButton.bind(self)} className='pe-jobstatus-page row'>
              <div className='col-md-3' >
                <span className='col-md-2'>{radioBtn}</span>
                <span className='col-md-2'>{imgTag}</span>
                <PE_tooltip position='right' content={fileName}>
                  <span className='col-md-8'>{fileName}</span>
                </PE_tooltip>
              </div>
              <div className='col-md-3'>{item.type}</div>
              <div className='col-md-3' >{item.modifiedBy}</div>
              <div className='col-md-3'>{getModifiedOn(item.modifiedOn)}</div>
              </div>)
            }
         }

     return (
          <div>
            {pageRender()}
          </div>
    );
  }
}

export default injectIntl(assets);
