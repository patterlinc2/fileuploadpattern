import React, { Component, PropTypes } from 'react';
import Assets from './assets';

class assetsList extends Component {
    render() {
        let cssGridLayout = this.props.cssGridLayout,
        setSelectedItem = this.props.setSelectedItem,
        selectedRecord = this.props.selectedRecord,
        self = this,
        styleName = '';


        if(this.props.pageView === 'grid-view'){
            styleName = 'resource'
        }
        let listItems = this.props.list.map(function (item) {
             return (
                <li key={item.nodeRef} className={styleName}>
                <div key={item.nodeRef} className={'col3-md1-'+cssGridLayout}>
                    <Assets
                            productTemp = {item}
                            selectedRecord = {selectedRecord}
                            setSelectedItem= {setSelectedItem}
                            pageView = {self.props.pageView}/>
                </div>
                </li>
            );
        });

        return (
            <div>
              {listItems}
            </div>
        )
    }
}


assetsList.propTypes = {
  cssGridLayout: PropTypes.number,
  setSelectedItem : PropTypes.func,
  selectedRecord: PropTypes.object,
  pageView: PropTypes.string,
  list: PropTypes.array
}


module.exports = assetsList;
