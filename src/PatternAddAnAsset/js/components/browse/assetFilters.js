import React, { Component, PropTypes } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import AssetsContainer from '../../container/AssetsContainer';
import {injectIntl, intlShape} from 'react-intl';
import {messages} from './assetFiltersDefaultMessages';

class AssetFilters extends Component {

  constructor(props) {
    super(props);
       this.state = {
        selectedTab: this.props.selectedIndex // Items per page
      }
    this.tabHandleSelect = props.tabHandleSelect.bind(this);
  }
  componentWillReceiveProps(nextProps){
     this.setState({
        selectedTab: nextProps.selectedIndex
     });
  }

  render() {
    const {formatMessage} = this.props.intl;
    return (
      <div className="pe-assetFilters">
         <Tabs  id="filterAssets" onSelect={this.tabHandleSelect} selectedIndex={this.state.selectedTab}>
          <TabList>
            <Tab selected>{formatMessage(messages.All_Heading)}</Tab>
            <Tab>{formatMessage(messages.Image_Heading)}</Tab>
            <Tab>{formatMessage(messages.Video_Heading)}</Tab>
            <Tab>{formatMessage(messages.Audio_Heading)}</Tab>
          </TabList>
          <TabPanel>
            <AssetsContainer filter="all"/>
          </TabPanel>
           <TabPanel>
            <AssetsContainer filter="image"/>
          </TabPanel>
          <TabPanel>
            <AssetsContainer filter="video"/>
          </TabPanel>
          <TabPanel>
            <AssetsContainer filter="audio"/>
           </TabPanel>
        </Tabs>
    </div>
    )
  }
}
AssetFilters.propTypes = {
        intl: intlShape.isRequired,
        selectedIndex:PropTypes.number,
        tabHandleSelect: PropTypes.func
  }


export default injectIntl(AssetFilters);
