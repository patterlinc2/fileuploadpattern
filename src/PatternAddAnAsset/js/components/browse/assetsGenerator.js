import React, { Component, PropTypes } from 'react';
import AssetsList from './assetsList';
import ItemsPerPage from '../pagination/itemsPerPage';
import Paginate from '../pagination/paging';
import Column from '../checkJobStatus/RenderColumn.js';
import Radio from '../common/Radio';
import ToolBar from './toolBar';
import Sort from '../common/SortAssets';

class assetsContainer extends Component {
        constructor(props) {
            super(props);
            this.state = {
                /*how many columns going to display*/
                column: '',
                /* grid column count for responsive design*/
                cssGridLayout:4,
                /* how many records going to show per page*/
                limitRecords: 4,
                selectedItem:null,
                pageView : this.props.pageDetails.viewName,
                viewName : 'grid-view',
                sortValue :''

            }
            this.onSelect = props.onSelect.bind(this);
            this.onChange = props.onChange.bind(this);
            this.saveSearch = props.saveSearch.bind(this);
            
        }

        changeView(view){ 
           this.setState({viewName:view});
           this.props.changeView(view);
        }

        onSort(sortoption){ 
            this.setState({sortValue:sortoption});
             this.props.onSort(sortoption, this.state.viewName);
        }


        render() {
                let limit = this.state.limitRecords,
                    dataArray,
                    size,
                    setSelectedItem = this.props.setSelectedItem,
                    selectedRecord = this.props.selectedRecord,
                    cssGridLayout = this.state.cssGridLayout,
                    resultsArray = [],
                    columnHeader = '',
                    pageView,
                    toolBar,
                    sortBar,
                    savedSearchLink= function (){return ''},
                    columns = ['Title', 'Type', 'Added by','Date Modified'],
                    sortData = ['Date Uploaded (Descending)',
                    'Date Uploaded (Ascending)',
                    'Name Ascending A-Z',
                    'Name Descending Z-A'];

                    dataArray = JSON.parse(JSON.stringify(this.props.assetsData));
            sortBar = <Sort sortOptions={sortData} change={this.onSort.bind(this)}/>
            toolBar = <ToolBar changeView={this.changeView.bind(this)}/>
            if(this.props.pageDetails.viewName === 'list-view' && dataArray != null){
                     size = 1;
                     pageView = 'list-view';
                     columnHeader = <Column cols={columns}/>
            }else{
                    size = 3;
                    pageView = 'grid-view';
            }

                while(dataArray && dataArray.length > 0)
                    resultsArray.push(dataArray.splice(0, size));

            let indents = resultsArray.map(function (i) {
                return (
                   <div key={i[0].nodeRef} className='row1'>
                        <AssetsList list = {i}
                                    selectedRecord= {selectedRecord}
                                    setSelectedItem= {setSelectedItem}
                                    cssGridLayout = {cssGridLayout}
                                    pageView = {pageView}
                                    />
                   </div>
                );
            });

            let statusBar = function () {return (
                <div className='sort-options'>
                <div className='pe-sortbox'>
                <span className='toolBar'>Sort:</span>
                {sortBar}</div>

            <div className='view-options'><span className='toolBar'> View:</span> {toolBar}</div>
            </div>)
            }
            if(this.props.saveSearchVisibility){
                let self = this;
                 savedSearchLink = function (){return (<div className="col-md-2 saveSearchLinkDiv">
                <a href="#" onClick={self.props.saveSearch}>Save search</a>
                </div>
                )}
            }
            let body = (
            <div>
                <div className='search-tab'>
                <div className='row'>
                <div className='col-md-10'>
                <ItemsPerPage pageView={pageView} itemsPerPageDetails={this.props.pageDetails} onChange={this.props.onChange}/>
                </div>
                {savedSearchLink()}
                </div>
                {statusBar()}
                    <div className='results-container'>
                        <ul>
                            {columnHeader}
                            {indents}
                        </ul>
                    </div>
                    <Paginate pageDetails={this.props.pageDetails}  onSelect={this.props.onSelect} />
                </div>
           </div>
        );

            let empty = < div className = 'alert alert-info' > No results found < /div>;
                return ( < div className = 'container'>
                    { resultsArray.length > 0 ? body : empty }
                </div>);
            }
        }

assetsContainer.propTypes = {
    cssGridLayout:PropTypes.number,
    pageDetails: PropTypes.object,
    itemsPerPageDetails:PropTypes.object,
    onSelect: PropTypes.func,
    onChange: PropTypes.func,
    onSort: PropTypes.func,
    saveSearch: PropTypes.func,
    changeView: PropTypes.func,
    setSelectedItem: PropTypes.func,
    selectedRecord: PropTypes.object,
    assetsData: PropTypes.array,
    saveSearchVisibility:PropTypes.bool,
    savedSearchLink:PropTypes.func
}

module.exports = assetsContainer;
