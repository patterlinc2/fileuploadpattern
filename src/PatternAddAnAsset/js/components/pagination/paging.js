import React, { Component, PropTypes } from 'react';
import Paginator from 'react-pagify';
import segmentize from 'segmentize';
import pagifyBootstrapPreset from './pagifyBootstrapPreset';
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../../constants/paginationConstants';

class Paginate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //pageLimit:9 // Items per page
      }
    };

  render() {
    const pageDetails = this.props.pageDetails
    let pageLimit = DEFAULT_MAX_RESULTS;
    if(this.props.pageDetails.pageLimit){
      pageLimit = this.props.pageDetails.pageLimit;
    }
    const paginated = paginate(pageDetails,pageLimit);
    const pages = Math.ceil(pageDetails.numberFound / Math.max(
        isNaN(pageLimit) ? 1 : pageLimit, 1)
    );

    let pagingHideclass = '';
    let disablePrevbutton = '';
    let disableNextbutton = '';

    /* hide pagination if there is only one page */
    if(pages ==1){
      pagingHideclass = 'paginghide'
    }

    /* disable next and previous buttons in pagination */
    if(pageDetails.pageNo == 1){
      disablePrevbutton = 'disablePageButton';
    }

     if(pageDetails.pageNo==pages){
        disableNextbutton = 'disablePageButton';
      }

      let libData;
      if(this.props.pageDetails.SearchValue){
      libData = ' for '+this.props.pageDetails.SearchValue;
    }else{
      let libData = ' in library : The Humanities: Culture, Continuity and Change, Volume II';
    }

    if(this.props.showDetail){
      let libData = '';
    }
    return (
      <div className='pagingdiv'>
      <div className='row'>
             <div className='col-md-8 pageDetail'>
            <span>Showing {pageDetails.index+1}-{pageDetails.index+pageDetails.totalRecords} of {pageDetails.numberFound} {libData}</span>
             </div>
                     <div className={'col-md-4 '+pagingHideclass}>
                    <Paginator.Context {...pagifyBootstrapPreset}
          segments={segmentize({
            page: pageDetails.pageNo,
            pages: pages,
            beginPages:DEFAULT_PAGE_NO,
            endPages: 1,
            sidePages: 1
          })} onSelect={this.props.onSelect}>
          <Paginator.Button className={disablePrevbutton} page={pageDetails.pageNo - 1}>«</Paginator.Button>

          <Paginator.Segment field='beginPages' />

          <Paginator.Ellipsis previousField='beginPages' nextField='previousPages' />

          <Paginator.Segment field='previousPages' />
          <Paginator.Segment field='centerPage' className='active' />
          <Paginator.Segment field='nextPages' />

          <Paginator.Ellipsis previousField='nextPages' nextField='endPages' />

          <Paginator.Segment field='endPages' />
          <Paginator.Button className={disableNextbutton} page={pageDetails.pageNo + 1}>»</Paginator.Button>

        </Paginator.Context>
        </div>
        </div>
      </div>
    );
  }
}

function paginate(PageDetails,pageLimit) {
  // adapt to zero indexed logic
  let page = PageDetails.pageNo - 1 || 0;
  let perPage = pageLimit;

  let amountOfPages = Math.ceil(PageDetails.numberFound / perPage);
  let startPage = page < amountOfPages? page: 0;

  return {
    amount: amountOfPages,
    page: startPage
  };
}

Paginate.propTypes = {
  pageDetails: PropTypes.object,
  showDetail: PropTypes.bool,
  onSelect: PropTypes.func
}

module.exports = Paginate;
