import React, { Component, PropTypes } from 'react';
import SelectBox from '../common/SelectBox';

class ItemsPerPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //pageLimit:4 // Items per page
      }
    };

  render() {
    let itemsPerPageListData=[
      {
         'value':'25',
         'text':'25'
      },
      {
         'value':'50',
         'text':'50'
      },
      {
         'value':'75',
         'text':'75'
      }
   ];

    let itemsPerPageThumbData=[
      {
         'value':'9',
         'text':'9'
      },
      {
         'value':'18',
         'text':'18'
      },
      {
         'value':'27',
         'text':'27'
      }
   ];

   let itemsPerPageData;

   if(this.props.pageView=='grid-view'){
    itemsPerPageData = itemsPerPageThumbData;
  }else{
   itemsPerPageData = itemsPerPageListData;
  }

    let itemsPerPageDetails = this.props.itemsPerPageDetails;
    let TotalCount = itemsPerPageDetails.numberFound;
    let displayItemCount = itemsPerPageDetails.displayItemCount;
    let SearchValue;
    if(this.props.itemsPerPageDetails.SearchValue){
      SearchValue = ' for '+this.props.itemsPerPageDetails.SearchValue;
    }

    return (
      <div>
        <div className='per-page-container'>
          Display&nbsp;
          <SelectBox id='itemPerPageSelectBox'
          value={displayItemCount}
          class='itemPerPageSelectBox' options={itemsPerPageData}
          onChange={this.props.onChange}/>
          <span> of {TotalCount} results {SearchValue}</span>
        </div>
      </div>
    );
  }
}

ItemsPerPage.propTypes ={
  pageView: PropTypes.string,
  onChange: PropTypes.func,
  itemsPerPageDetails:PropTypes.object
}




module.exports = ItemsPerPage;
