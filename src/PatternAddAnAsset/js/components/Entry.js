import React, { Component, PropTypes } from 'react';
import { Link, browserHistory } from 'react-router';
import assetsContainer from '../container/assetsContainer';
import Popup from './EntryPopup';

Entry.propTypes = {
    children: PropTypes.object
}

export default function Entry({ children }) {

 let child = <div style={{ marginTop: '1.5em' }}>{children}</div>;

  return (
    <div>
    	<Popup children = {children}/>
    </div>
  )
}
