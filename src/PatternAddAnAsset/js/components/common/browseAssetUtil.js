import TreeNodeUtil from '../folderpane/TreeNodeUtil';

export function getNodeRef(nodeRef){
		let ref;
		if(nodeRef){
			let nodes = nodeRef.split('/');
			if(nodes.length > 0){
				ref = nodes[nodes.length-1];
			}
		}

		return ref;
		// this.props.parent(self,1);
}

export function highlightChildren(model, fileName){
	let childrens = document.querySelectorAll('.filter-container:first-child span');
		childrens.forEach(function (child) {
			if (child.getAttribute('name') === fileName) {
				child.className = 'pe_filter_enabled link-view';
			}

		});
}

export function recentlySelectedChild(nodeRef){
let childrens = document.querySelectorAll('.filter-container div div span');
  childrens.forEach(function (child){
    if(child.id != nodeRef){
      child.classList.remove('tree-node-selected');
    }
  });
}

export function getFirstObj(data){
  let node;
  for(let obj in data){
     node = data[obj];
    if(obj == 0){
      return node;
      break;
    }
  }
}

export function getNodeRefValue(obj){
  if(obj != undefined){
    return obj.nodeRef;
  }
}

export function trimFolderName(fileName){
	if (fileName){
			if (fileName.length > 28) {
				fileName = fileName.substring(0,10) + '...';
			}
		}
		return fileName;
}

export function getFirstName(data){
  let name;
  for(let obj in data){
    let node = data[obj];
    if(obj == 0){
      name = node.fileName;
      break;
    }
  }
  return name;
}

export function getModifiedOn(date){
  const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  let d = new Date(date);
  let modifiedOn,
  dat,
  month,
  year;
  dat = d.getDate();
  month = d.getMonth();
  year = d.getFullYear();

  for(let index in months){
      if(month == index){
        month = months[index];
        break;
      }
  }

modifiedOn = month+' '+dat+' '+year;
return modifiedOn;
}

export function getAssetData(dataArray){
  let assetItem;
  let assetArray = [];
  dataArray = JSON.parse(JSON.stringify(dataArray));
  if(dataArray && dataArray.items.length > 0){
    let items = dataArray.items;
    for(let obj in items){
      assetItem = items[obj];
      if(assetItem.type != 'forumpost'){
        assetArray.push(assetItem);
     }
   }
   dataArray.items = assetArray;
   // console.log(dataArray);
 }
 return dataArray;
}
