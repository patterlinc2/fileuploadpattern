import React, { Component, PropTypes } from 'react';

class Label extends Component{
	constructor(props) {
	    super(props);
	    this.displayName = 'Label';
  	}
    static propTypes = {
        for: PropTypes.string,
        text : PropTypes.string.isRequired,
        className: PropTypes.string
    }
    static defaultProps ={
          for:'',
          text: ''
    }
    render() {
        let forTxt = this.props.for;
        let text = this.props.text;

        //below is the pearson label
       // let divAsLabel = "<div class='pe-label pe-label--bold' >{text}</div>";
        return (
            <label className= {this.props.className} htmlFor={forTxt}> {text} </label>
        );
    }

};

module.exports = Label;
