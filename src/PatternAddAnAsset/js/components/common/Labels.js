import React, { Component, PropTypes } from 'react';

class Labels extends Component{

render(){

	let labelName = this.props.label;
	let underLine = this.props.underline;
	if(underLine === 'true'){
		labelName = <u>{labelName}</u>;
	}else{
		labelName = labelName;
	}

	return(
			<div className='clabelOrg'>
				{labelName}
			</div>

		);
}

}

Labels.propTypes = {
	label: PropTypes.string,
	underline: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number,
    ])
}

module.exports= Labels;
