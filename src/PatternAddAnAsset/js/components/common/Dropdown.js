import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

class DropDown extends Component{

    constructor(props){
        super(props);
         this.state={
            listVisible: false,
            lists : this.props.selected
        };
        this.show = this.show.bind(this);
        this.hide = this.hide.bind(this);
        this.select = this.select.bind(this);
    }
    select(item) {
        console.log(item.value);
        this.setState({lists:item});
        this.props.onChange(item.value);
    }

    show() {
        this.setState({ listVisible: true });
        document.addEventListener('click', this.hide);
    }

    hide() {
        this.setState({ listVisible: false });
        document.removeEventListener('click', this.hide);
    }

    renderListItems(selectedName) {
        let items = [];
        let item;
        for (let i = 0; i < this.props.list.length; i++) {
            item = this.props.list[i];
            if(selectedName != item.name){
                items.push(<div className={item.value} key={item.name} onClick={this.select.bind(null, item)}>
                    <span><i className={item.name}></i></span>
                </div>);
            }
        }
        return items;
    }

    render() {
        return (<div className={'dropdown-container' + (this.state.listVisible ? ' show' : '')}>
            <div className={'dropdown-display' + (this.state.listVisible ? ' clicked': '')}
            onClick={this.show.bind(this)}>
                <span><i className={this.state.lists.name}></i></span>
                <i className='fa fa-angle-down'></i>
            </div>
            <div className='dropdown-list'>
                <div className='dropdown-item'>
                    {this.renderListItems(this.state.lists.name)}
                </div>
            </div>
        </div>)
    }


}

DropDown.propTypes = {
    selected : PropTypes.object,
    list : PropTypes.array,
    onChange : PropTypes.func,
}
module.exports= DropDown;
