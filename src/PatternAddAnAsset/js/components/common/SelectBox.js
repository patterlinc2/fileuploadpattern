import React, { Component, PropTypes } from 'react';

class SelectBox extends Component{
constructor(props) {
    super(props);
    this.displayName = 'SelectBox';
    this.handleChange = this.handleChange.bind(this);
}
static propTypes= {
		id:PropTypes.string.isRequired,
		multiple: PropTypes.bool,
		disabled: PropTypes.bool,
		required: PropTypes.bool,
		autofocus: PropTypes.bool,
		defaultValue:PropTypes.string,
        options:PropTypes.array.isRequired,
        defaultData: PropTypes.object,
        onChange: PropTypes.func,
        value: PropTypes.number,
        class: PropTypes.string,
}
static defaultProps ={
		  id:'',
		  multiple: false,
		  disabled:false,
		  required:false,
		  autofocus: false,
		  defaultValue:'',
          options:[],
          className:''
}
componentWillMount() {
        let defaultValue = this.props.defaultData ;
        if(this.props.options!==undefined && this.props.options.length>0){
            this.props.options.map(function (CurrOption){
               if(defaultValue === CurrOption.text){
                    CurrOption.selected = 'selected';
                }else{
                    CurrOption.selected = '';
                }
            });
        }
}
componentDidMount() {
		if (this.props.autofocus){
			//this.refs.select.focus();
		}
}

componentWillReceiveProps() {
    // let defaultValue = this.props.defaultData;
    //     if(this.props.options!==undefined && this.props.options.length>0){
    //         this.props.options.map(function (CurrOption){
    //            if(defaultValue === CurrOption.text){
    //                 CurrOption.selected = 'selected';
    //             }else{
    //                 CurrOption.selected = '';
    //             }
    //         });
    //     }
}

handleChange(e) {
    this.props.onChange(e);
        // if (this.props.handleChange) {
        //     this.props.handleChange({"id":e.target.id,"value":e.target.value.trim()});
        // }
}
render() {
        return (
            <select value={this.props.value} className={this.props.class} id={this.props.id} multiple={this.props.multiple} disabled={this.props.disabled} required={this.props.required}
			ref="select" onChange ={this.handleChange}>
                {this.props.options.map((CurrOption, index) =>
                <option value = {CurrOption.value}  key={CurrOption.value} value={CurrOption.selected}>{CurrOption.text}</option>
            )}
            </select>
        )
    }

};

module.exports = SelectBox;
