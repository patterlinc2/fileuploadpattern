import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';

class SortAssets extends Component{

	constructor(){
		super();
		this.state={
			value:''
		}

	}

	change(event){
         this.setState({value: event.target.value});
         this.props.change(event.target.value);
     }

     assignKeys(sortValue){
     	if(sortValue == 'Date Uploaded (Descending)'){
     		return 'cm:created|false'
     	}else if(sortValue == 'Date Uploaded (Ascending)'){
            return 'cm:created|true';
     	}else if(sortValue == 'Name Ascending A-Z'){
     		return 'cm:name|true';
     	}else if(sortValue == 'Name Descending Z-A'){
     		return 'cm:name|false';
     	}

     }

     getSelect(options, self){
     	let sortView = (<select id="sort" 
     						   className='itemPerPageSelectBox' 
     						   onChange={self.change.bind(self)} 
     						   value={self.state.value}>{options}
     						   </select>);

     	return sortView;
     }
	render(){
		let self = this;
		let sorting = new Map();
		let sortOption = self.props.sortOptions;
		let options = sortOption.map(function (sortData, index){
						sorting.set(sortData, self.assignKeys(sortData));
                  	  return <option key={sortData} value={sorting.get(sortData)}>{sortData}</option>
                  	});

        let sortView = this.getSelect(options,self);

		return(
				<div className="sort-asset">
						{sortView}
				</div>

		)
	}

}
SortAssets.propTypes={
 sortOptions: PropTypes.array,
 change: PropTypes.func
};

module.exports= SortAssets;
