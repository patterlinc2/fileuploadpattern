import React, { Component, PropTypes } from 'react';

class AccordionSection extends Component{

    constructor(props) {
    super(props);
    this.displayName = 'AccordionSection';
    this.onSelect = this.onSelect.bind(this);

 }
 onSelect(event) {
    event.preventDefault();
        // tell the parent Accordion component that this section was selected
        this.props._onSelect(this.props.id);
    }


    render() {
        let className = 'accordion-section' + (this.props._selected ? ' selected' : '');

        return (
            <div className={className}>
            <div className="sec">
                <a href="#" onClick={this.onSelect}>
                    {this.props.title}
                </a>
            </div>
                <div className="body">
                    {this.props.children}
                </div>
            </div>
        );
    }

};

AccordionSection.propTypes = {
    _onSelect: PropTypes.func,
    _selected: PropTypes.bool,
    id: PropTypes.string,
    title:  PropTypes.string,
    children: PropTypes.object
}

module.exports = AccordionSection;
