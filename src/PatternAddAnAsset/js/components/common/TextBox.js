import React, { Component, PropTypes } from 'react';

class TextBox extends Component{

constructor(props) {
    super(props);
    this.displayName = 'TextBox';
 }

render() {

  const inputState = (value) => {
      if (this.props.value.touched && this.props.value.error) {
        return 'pe-input--error'
      } else if (this.props.value.touched && this.props.value.value) {
        return ''
      } else {
        return ''
      }
    }

        return (
            <input {...this.props.value}
            id={this.props.id}
            className= {this.props.className +' '+ inputState(this.props.value)}
              ref="input" type="text"
               required={this.props.required}
               maxLength={this.props.maxLength}
               placeholder={this.props.placeholder}
               readOnly={this.props.readOnly}
               disabled={this.props.disabled}
              
            />
        )
    }

};

TextBox.propTypes = {
    id:PropTypes.string,
    value: PropTypes.object,
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    required: PropTypes.bool,
    maxLength:PropTypes.string,
    autofocus: PropTypes.bool,
    className: PropTypes.string,
    readOnly: PropTypes.bool
}

module.exports = TextBox;
