import React, { Component, PropTypes } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import FileUploadContainer from '../container/FileUploadContainer';
import CheckJobStatusContainer from '../container/CheckJobStatusContainer';
import UploadProgressContainer from '../container/UploadProgressContainer';
import SearchLibrary from './SearchLibrary';
import BrowseAssetsContainer from '../container/BrowseAssetsContainer';
import SearchLibraryContainer from '../container/SearchLibraryContainer';
import { Link, browserHistory, hashHistory } from 'react-router';
class AddAnAsset extends Component {

  constructor(props) {
    super(props);
    this.onSave = props.onSave;
  }

  handleSelect(index, last) {
    sessionStorage.currentTab= index;
    hashHistory.push('/');
  }

  render() {
  let isError = true;
  let indexVal;
  if(!sessionStorage.currentTab){
    indexVal=0;
    sessionStorage.currentTab=indexVal;
  }else{
    indexVal = parseInt(sessionStorage.currentTab);
  }

  let fileuploadContainer = <FileUploadContainer />;
  let checkJobStatus = <CheckJobStatusContainer />;
  let uploadProgress = <UploadProgressContainer />;

  if(this.props.children != null){
    fileuploadContainer = '';
	   	if(this.props.children.props.location.pathname == '/UploadInProgress'){
	   	checkJobStatus = '';
	    }
	    if(this.props.children.props.location.pathname == '/CheckJobStatus'){
	   	  uploadProgress = '';
	    }

	    if(this.props.children.props.location.pathname == '/errorUploading'){
          checkJobStatus = '';
          uploadProgress = <UploadProgressContainer error={isError} />;
        }


  }else{
  	checkJobStatus = '';
  	uploadProgress = '';
  }
    return (
      <div className="pe-uploadasset">
         <Tabs id="addAnAssets" onSelect={this.handleSelect} selectedIndex={indexVal}>
          <TabList className="parentTab">
            <Tab>Search Library</Tab>
            <Tab>Browse Assets</Tab>
            <Tab>Upload Files</Tab>
          </TabList>
          <TabPanel>
           <SearchLibraryContainer />
          </TabPanel>
          <TabPanel className="browseAssets">
           <BrowseAssetsContainer />
          </TabPanel>
        <TabPanel className="uploadAssets">
          <div>
          {fileuploadContainer}
          </div>
          <div>
          {checkJobStatus}
          </div>
          <div>
          {uploadProgress}
          </div>
       </TabPanel>
      </Tabs>
    </div>
    )


  }
}

AddAnAsset.propTypes = {
  children: PropTypes.object,
  onSave: PropTypes.func
}

module.exports = AddAnAsset;
