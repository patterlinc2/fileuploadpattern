import React from 'react';
import AddAnAsset from './AddAnAsset'
import Modal from 'react-modal';

class EntryPopup extends React.Component{

	constructor(props){
		super(props);
		this.state={
			modalIsOpen : true
		}
		this.closeModal = this.closeModal.bind(this);
	}

	closeModal(){
		this.setState({modalIsOpen : false});
	}

	render(){
		return(
			<div>
				
				<Modal isOpen = {this.state.modalIsOpen}>
					<div className='row modalHeadDiv'>
						<div className='col-md-11'>Add an Asset</div>
						<div className='col-md-1 closeButtonDiv'>
						<i className='fa fa-times' aria-hidden='true' 
						onClick={this.closeModal} ></i></div>
						</div>
					<div className='modalBodyDiv'>
		          		<AddAnAsset children={this.props.children} />
		          	</div>
	        	</Modal>
			</div>

			)
	}
}

EntryPopup.propTypes = {
	children : React.PropTypes.object
}
export default EntryPopup;
