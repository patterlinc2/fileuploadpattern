import React, { Component, PropTypes } from 'react';
import AddAnAsset from './AddAnAsset';
import AssetFiltersContainer from '../container/AssetFiltersContainer';
import FolderTree from '../container/TreePaneContainer';

class BrowseAssets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            record: this.props.record,
        };
        this.sendToQuad = props.sendToQuad.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.state.record = nextProps.record;
    }

    render() {
      return (
        <div className="search-tab browse-library">
            <div>
              <b>Browsing in : </b>The Humanities, Culture, Continuity and Change, Volume II
            </div>
            <div className="row main">
                <div className="filter-container">
                <div className="filters-label">Documents</div>

                    <FolderTree/>
                </div>
                <div className="pe-search-results-container browse-tab-results">
                    <AssetFiltersContainer />
                </div>
                <div className="pe-btn-bar">
                    <button className="pe-btn pe-cancel-btn" type="button">Cancel</button>
                    <button className="pe-btn pe-btn--primary selectBtn" onClick={this.sendToQuad} type="button">Select</button>
                </div>
            </div>
        </div>
      )
    }
}

BrowseAssets.propTypes = {
    record: PropTypes.object,
    sendToQuad: PropTypes.func
}

module.exports = BrowseAssets;
