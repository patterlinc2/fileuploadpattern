import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Label from'./common/Label';

class SearchFilter extends Component{

        constructor(props){
        super(props);
            this.state = {
        showToggle: true
    }
         this.ShowContextSpecific =  this.ShowContextSpecific.bind(this);

    }

    ShowContextSpecific(e){
   this.setState({showToggle: !this.state.showToggle})
}

        render(){
            return (
                    <div id="section">
                    <Label className="LabelHead" for="Description" text="Search Filters"/>
                    <Label className="LabelBold" for="Description" text="Difficulty Level"/>
                    <Label for="Description" text="Easy(25)"/>
                    <Label for="Description" text="Medium(22)"/>
                    <Label for="Description" text="Diffcult(41)"/>
                     </div>
                )
            }
    }

module.exports= SearchFilter;
