import React, { Component, PropTypes } from 'react';
import { Link, browserHistory } from 'react-router';
import TableComponent from './tableComponent';
import AssetFiltersContainer from '../../container/AssetFiltersContainer';
import Paginate from '../pagination/paging';

class SavedSearchComponent extends Component {

    constructor(props) {
        super(props);
        this.displayName = 'ReviewAssetMedataComp';
        this.componentWillMount = props.componentWillMount;
        this.handleChange = props.handleChange.bind(this);
        this.onSelect = props.onSelect.bind(this);
        this.deleteSavedSearch = props.deleteSavedSearch;
        this.runSavedSearch = props.runSavedSearch;
    }

    render() {
    	let columns = ['Search Term', 'Filters'];
      let self = this;
      let CheckedValues = this.props.CheckedValues;
      let rows = this.props.rows;
      let isShowDetail = true;
      if(CheckedValues.length>0){
        for(let i=0;i<rows.length;i++){
          for(let j=0;j<CheckedValues.length;j++){
            if(rows[i].id==CheckedValues[j].id){
              rows[i].isChecked = CheckedValues[j].checked;
            }
          }
        }
      }
      let body = (
        <div className="table">
                <div className="col-md-10">
                <TableComponent className="jobScheduleTable"
                  columns={columns}
                  rows={rows}
/*                  enableDelete={this.props.enableDelete}
                  enableSearch={this.props.enableSearch}*/
                   checkboxOnchange={this.props.handleChange}/>


                </div>
                 <Paginate pageDetails={this.props.pageDetails} showDetail = {isShowDetail}  onSelect={this.props.onSelect} />
                   <div className= "pe-btn-bar savedSearchBar">
                        <button className="pe-btn runSearchBtn" disabled={!this.props.enableSearch}
                        onClick={this.props.runSavedSearch}>Run Search Again</button>
                        <button className="pe-btn deleteSearch" disabled={!this.props.enableDelete}
                        onClick={this.props.deleteSavedSearch}>Delete Search</button>
                </div>
        </div>
        )
            let empty = < div className = "alert alert-info" > No results found < /div>;

      return (
          <div>
              { this.props.rows.length > 0 ? body : empty }
            </div>
      );
    }
}

SavedSearchComponent.propTypes = {
  handleChange: React.PropTypes.func,
  componentWillMount: React.PropTypes.func,
  onSelect: React.PropTypes.func,
  deleteSavedSearch: React.PropTypes.func,
  runSavedSearch: React.PropTypes.func,
  enableDelete: React.PropTypes.func,
  enableSearch: React.PropTypes.func,
  pageDetails: React.PropTypes.object,
  CheckedValues: React.PropTypes.object,
  rows: React.PropTypes.object,
  columns: React.PropTypes.object
}

module.exports = SavedSearchComponent;
