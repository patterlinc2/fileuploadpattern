import React, { Component, PropTypes } from 'react';
import Row from './RenderRow.js';
import Column from './RenderColumn.js';

class tableComponent extends Component {

  constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

  handleChange(obj){
    if(this.props.checkboxOnchange){
            this.props.checkboxOnchange(obj);
    }
  }
  render(){
    let column = this.props.columns.map(function (column,index){
    return column;
  });
    return(
      <div>
        <Column cols={this.props.columns}/>
        <Row rows={this.props.rows} CheckboxHandler={this.handleChange}/>
        </div>

      );
  }
}

tableComponent.propTypes = {
  checkboxOnchange: PropTypes.func,
  rows: PropTypes.object,
  columns: PropTypes.object
}

module.exports= tableComponent;
