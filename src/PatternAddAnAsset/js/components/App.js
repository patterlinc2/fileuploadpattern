import React, {Component, PropTypes} from 'react';
import Entry from './Entry'
import { Provider } from 'react-redux'
import { render } from 'react-dom'
import store from '../store'
import { Router, Route, IndexRoute, browserHistory, hashHistory } from 'react-router'
import FileUploadContainer from '../container/FileUploadContainer'
import CheckJobStatus from '../container/CheckJobStatusContainer';
import UploadInProgressContainer from '../container/UploadProgressContainer';
import AssetsContainer from '../container/AssetsContainer';
import {addLocaleData, IntlProvider} from 'react-intl';
import SearchLibraryContainer from '../container/SearchLibraryContainer';
import frLocaleData from 'react-intl/locale-data/fr';
import { syncHistoryWithStore} from 'react-router-redux'
import SingleFileFolderContainer from '../container/SingleFileFolderContainer';

const localforage = require('localforage');

addLocaleData(frLocaleData);
const translations = {
  'fr' : {
       'Review_Asset_MetaData': 'Métadonnées du produit',
      }
};
//const locale =  document.documentElement.getAttribute('lang');
const locale = 'en';

const history = syncHistoryWithStore(browserHistory, store);



localforage.getItem('last_three_search').then(function (data){
  console.log('last_three_search');
  console.log(data);
  if(data == null){
      localforage.setItem('last_three_search',[]).then(function (data){
       console.log(data);
      })
  }
})

class App extends Component {

  render() {
    return (
      <IntlProvider locale={locale} messages={translations[locale]}>
         <Provider store={store}>
           <div>
            <Router history={hashHistory}>
              <Route path='/' component={Entry}>
                <Route path='CheckJobStatus' component={CheckJobStatus}/>
                <Route path='UploadInProgress' component={UploadInProgressContainer}/>
                 <Route path='errorUploading' component={UploadInProgressContainer}/>
                <Route path='assets' component={AssetsContainer}/>
              </Route>
            </Router>
          </div>
        </Provider></IntlProvider>
      );
  }
}

export default App;
