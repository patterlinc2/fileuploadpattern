import { DISPLAY_ASSETS,UPDATE_ASSET_TAB_INDEX} from '../constants/fileUploadConstants'
import { SEARCH_DISPLAY_ASSETS} from '../constants/searchLibraryConstants'
import Immutable from 'immutable';
import serviceUrls from '../constants/service';
import {format} from '../utils/stringManipulation';

let initilizeValues = Immutable.List.of([{}]);

const assets = (state = initilizeValues, action)=>{

  switch(action.type) {
    case DISPLAY_ASSETS:
    let temp = action.data.items;
    for (let i in temp) {
      let temp1 = temp[i].nodeRef.split('/');
      let refId = temp1[temp1.length -1];
      let imageUrl = format(serviceUrls.thumbnail+'?alf_ticket={1}', [refId, action.data.token]);
      temp[i].url = imageUrl;
    }
    return state.push(action.data);
    break;
    default:
    return state
    }
  }

  module.exports= assets;
