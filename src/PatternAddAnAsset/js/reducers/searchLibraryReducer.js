import { SAVED_SEARCH_VALUE,GET_ASSERTS_DATA,
         UPDATE_CHECKBOX_VALUE,
         SEARCH_BUTTON_VISIBILITY} from '../constants/searchLibraryConstants'
import Immutable from 'immutable'

const initilizeValues = {
   savedSearchValue: [],
   enableDelete:false,
   enableSearch:false,
   isSavedSearch: true
}

const searchLibraryReducer = (state = initilizeValues, action)=>{
  switch(action.type) {


    case SAVED_SEARCH_VALUE:
    if(!state && !state[0]){
    return[
       Immutable.fromJS(state[0]).merge(Immutable.Map(action.fields)).toJS()
    ]}
    else{
      return[
        Immutable.fromJS({
          savedSearchValue: [],
          enableDelete:false,
          enableSearch:false,
          isSavedSearch: true
        }).merge(Immutable.Map(action.fields)).toJS()
      ]
    }

    case GET_ASSERTS_DATA:
    return [
      ...state,action
    ]
    case SEARCH_BUTTON_VISIBILITY:
    return[
      ...state,action.isSavedSearch
    ]

    default:
    return state

  }
}

module.exports= searchLibraryReducer;
