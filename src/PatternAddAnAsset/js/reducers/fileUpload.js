import { FILE_SAVE} from '../constants/fileUploadConstants'
import Immutable from 'immutable'

const initilizeValues = [{
  name: '',
  file: {}
}];

const fileUpload = (state = initilizeValues, action)=>{

  switch(action.type) {

    case 'FILE_SAVE':
     if(!state && !state[0]){
    return[
       Immutable.fromJS(state[0]).merge(Immutable.Map(action.fields)).toJS()
    ]
    }
    else{
      return[
        Immutable.fromJS({
  name: '',
  file: {}
}).merge(Immutable.Map(action.fields)).toJS()
      ]
    }
    case 'UPLOAD_STATUS':
    return[
       ...state,action
    ]
    default:
    return state

  }
}

module.exports= fileUpload;
