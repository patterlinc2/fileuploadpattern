import { GET_FOLDER, TOGGLE} from '../constants/TreePaneConstant'

let initilizeValues = [[{
        items: '',
        expanded: true
}]];

const folderTree = (state = initilizeValues, action)=>{

  switch(action.type) {
    case GET_FOLDER:
    return  [
          ...state, action.data
    ]
    break;

    default:
    return state
    }
  }

  module.exports= folderTree;
