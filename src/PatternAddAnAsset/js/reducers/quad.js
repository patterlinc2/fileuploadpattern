import { SEND_TO_QUAD} from '../constants/fileUploadConstants'
import Immutable from 'immutable'

let initilizeValues = Immutable.List.of([]);

const sendToQuad = (state = initilizeValues, action)=>{

  switch(action.type) {
    case SEND_TO_QUAD:
      return state.push(action.data);
    break;

    default:
     return state

    }
  }

  module.exports= sendToQuad;
