import { connect } from 'react-redux';
import { populateAutoComplete } from '../action';
import { getSearchProductItems } from '../action/SearchLibraryAction';
import SearchComplete from '../components/SearchComplete';
import _ from 'lodash';
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';

const localforage = require('localforage');

const getSelectedValues = (dataArray) => {
  let currentData = '';
  if(dataArray){
	  if (dataArray.length > 0) {
	     currentData = dataArray[dataArray.length-1];
	      if(!_.isEmpty(currentData)){

	      	    let  autosectionArray = [{
	      	      			title: '',
						    suggestions: currentData.data['suggestions']
						   }
	      	                ];

	      	              /*autosectionArray.push({
	      	      			title: '',
						    suggestions: currentData.data['suggestions']
						   })*/

				if(currentData.lastThreeSearch.length >=3){
                 autosectionArray.push({
						    title: 'Recent Search Terms',
						    suggestions: currentData.lastThreeSearch
						    })
                }

                if(currentData.savedSearch.length >= 1){
                 autosectionArray.push({
						    title: 'Saved searches',
						    suggestions: currentData.savedSearch
						   })
                }

                 let data = autosectionArray;
	      	      /*let data = [
	      	      			{
	      	      			title: '',
						    suggestions: currentData.data['suggestions']
						   },
	      	                {
						    title: 'Last three search terms',
						    suggestions: currentData.lastThreeSearch
						    },
						    {
						    title: 'Saved searches',
						    suggestions: currentData.savedSearch
						   }];*/
		    return data;
		}else{
			return [];
		}
	  }
  }

  return [];


}


const mapStateToProps = (state) => {

let searchRes = getSelectedValues(state.autoComplete);
let searchText = state.autoComplete[state.autoComplete.length-1].text;

return {
	data:{
		data:searchRes,
		text:searchText
	}
}
}


const mapDispatchToProps = (dispatch) => {
  return {
  	onSuggestionSelected: (event, { suggestion, suggestionValue, sectionIndex, method }) => {
       console.log('onSuggestionSelected');
       console.log(suggestionValue);

		localforage.getItem('last_three_search').then(function (lastvalue){
			console.log(lastvalue);
			console.log(suggestionValue);
			if(suggestionValue != undefined && suggestionValue != ''){
				let chkVal = _.find(lastvalue, { 'term': suggestionValue});
				if(chkVal == undefined){
					let sval = {term:suggestionValue};
					console.log(sval);
					if(lastvalue.length >= 3){
						lastvalue.pop(lastvalue.unshift(sval));
					}else{
						lastvalue.unshift(sval);
					}
				}

			}
			let viewName = '';
			if(document.querySelector('.dropdown-display span i')){
				if(document.querySelector('.dropdown-display span i').className=='fa fa-list'){
		        	viewName = 'list-view';
		    	}else{
		        	viewName = 'grid-view';
		     	}
		      }

			localforage.setItem('last_three_search', lastvalue, function (err, val) {
			console.log(val);
			dispatch(getSearchProductItems(suggestionValue,DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS,'','',viewName));
			document.querySelectorAll('#displayContainerDiv')[0].style.display = 'block';

				let selectedTab = document.querySelector('#filterAssets .ReactTabs__Tab--selected').textContent;
				if(selectedTab == 'Saved Search'){
					document.querySelectorAll('#filterAssets .ReactTabs__Tab')[0].click();
				}

			});

		})


  	},

    onSuggestionsUpdateRequested: ({value}) => {

	localforage.getItem('savedSearch').then(function (searchvalue) {
	    // This code runs once the value has been loaded
	    // from the offline store.
	    let sarr = [];
	    if(searchvalue != null){

		    console.log(searchvalue);

	  		_(searchvalue).forEach(function (data) {
		  		console.log(data);
		  		sarr.push({term:data.searchterm});
			});
			console.log(sarr);
	    }

        localforage.getItem('last_three_search').then(function (lastthree){
            let searchdata = [];
            /*if(lastthree.length >=3){
              searchdata =  _.takeRight(lastthree,3);
            }*/

            searchdata =  _.takeRight(lastthree,3);

       		dispatch(populateAutoComplete(value,sarr,searchdata));
        });

	}).catch(function (err) {
	    // This code runs if there were any errors
	    console.log(err);
	});



     console.log(value);
    }

  }
}

const SearchCompleteContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchComplete)

export default SearchCompleteContainer
