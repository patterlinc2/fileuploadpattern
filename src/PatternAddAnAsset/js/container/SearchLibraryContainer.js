import {connect} from 'react-redux';
import SearchLibrary from '../components/SearchLibrary';
import { getSavedSearchValues,
  getSearchProductItems,
  saveSearchValues,
  deleteSavedSearch,
  runSearch } from '../action/SearchLibraryAction';
import { Link, browserHistory, hashHistory } from 'react-router'
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';
import _ from 'lodash';
import {getCurrentValues} from '../utils/util';

const localforage = require('localforage');

const getSelectedValues = (dataArray) => {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }

  return [];
}

const mapStateToProps = (state) => {
    let productName = '';
    let data = getSelectedValues(state.searchLibraryReducer);
    let selectedData = getCurrentValues(state.quad);

    if(state.autoComplete.length > 0){
        productName = state.autoComplete[state.autoComplete.length-1].text;
     }
     return {
     enableSearch: state.searchLibraryReducer.enableSearch,
    enableDelete: state.searchLibraryReducer.enableDelete,
    isSavedSearch: data.isSavedSearch,
    record: Array.isArray(selectedData)? {}: selectedData,
    showAssets:false,
    'SearchValue':productName,
    'initialValues': {
            productName:productName

          }
        }
}

const mapDispatchToProps = (dispatch) => {
  return {
  		componentWillMount() {
  			getSavedSearchValues();
  		},
      componentWillReceiveProps(){

      },
      getSearchProduct(value,dispatch){
      //dispatch(getSearchProductItems(value.productName,1));
      	let searchValue = value;

        localforage.getItem('last_three_search').then(function (lastvalue){
        console.log(lastvalue);
        console.log(searchValue);

        if(searchValue.productName != undefined && searchValue.productName != ''){
        	let chkVal = _.find(lastvalue, { 'term': searchValue.productName});
            if(chkVal == undefined){
            	let sval = {term:searchValue.productName};
            	console.log(sval);
	            if(lastvalue.length >= 3){
				   lastvalue.pop(lastvalue.unshift(sval));
	            }else{
	               lastvalue.unshift(sval);
	            }
            }

        }

			localforage.setItem('last_three_search', lastvalue, function (err, val) {
				console.log(val);
		        dispatch(getSearchProductItems(searchValue.productName,DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS));
		        document.querySelectorAll('#displayContainerDiv')[0].style.display = 'block';

				let selectedTab = document.querySelector('#filterAssets .ReactTabs__Tab--selected').textContent;
					if(selectedTab == 'Saved Search'){
						document.querySelectorAll('#filterAssets .ReactTabs__Tab')[0].click();
					}

			});

      })
   },

    sendToQuad: function (record) {
      let temp = JSON.stringify(this.props.record);
      console.log(temp);
      alert(temp);
    }

  }
	}

const SearchLibraryContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchLibrary)

export default SearchLibraryContainer;
