import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {fetchingAssets} from '../action/assets';
import BrowseAssets from '../components/BrowseAssets';
import {getCurrentValues} from '../utils/util';

const mapStateToProps = (state) => {
 let data = getCurrentValues(state.quad)
  return {
    record: Array.isArray(data)? {}: data
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    sendToQuad: function (record) {
      let temp = JSON.stringify(this.props.record);
      console.log(temp);
      alert(temp);
    }
  };
}

const BrowseAssetsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(BrowseAssets)

export default BrowseAssetsContainer;
