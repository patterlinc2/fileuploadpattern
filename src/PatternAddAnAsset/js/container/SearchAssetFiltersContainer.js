import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {getSearchProductItems, searchLibButtonVisibility} from '../action/SearchLibraryAction';
import {fetchSavedSearchData} from '../action/savedSearchAction';
import searchAssetsFilter from '../components/browse/searchAssetsFilter';
import serviceUrls from '../constants/service';
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';
import { DISPLAY_ASSETS,UPDATE_ASSET_TAB_INDEX} from '../constants/fileUploadConstants'

const getSelectedValues = (dataArray) => {
  if (dataArray.size > 1) {
    let latestItem = dataArray.size-1;
    return dataArray.get(latestItem);
  }

  return [];
}

const mapStateToProps = (state) => {
   let data = getSelectedValues(state.searchAssets);
  return {
    selectedIndex:data.selectedIndex
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    tabHandleSelect: function (index, last) {
      sessionStorage.AssetTabIndex = index;
      if(index==4){
         dispatch({
           type : UPDATE_ASSET_TAB_INDEX,
           data : index
        });
      }
      let filterUrlsForSearch = {
        0:serviceUrls.searchBasedAll,
        1:serviceUrls.searchBasedImage,
        2:serviceUrls.searchBasedVideo,
        3:serviceUrls.searchBasedAudio
      };


      if(index!==4){
      let searchValue = document.querySelector('#addAnAssets .react-autosuggest__input').value;
      dispatch(getSearchProductItems(searchValue,DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS,filterUrlsForSearch[index]));
      }else{
        dispatch(fetchSavedSearchData(DEFAULT_PAGE_NO));
      }

      if(index === 4){
        dispatch(searchLibButtonVisibility(true));
      }else{
        dispatch(searchLibButtonVisibility(false));
      }
    }
  }
}

const SearchAssetFiltersContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(searchAssetsFilter)

export default SearchAssetFiltersContainer;
