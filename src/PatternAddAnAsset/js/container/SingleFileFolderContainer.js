import React from 'react';
import { connect } from 'react-redux';
import {fetchingAssets} from '../action/assets';
import {getFolders,toggle, setReference} from '../action/TreePaneAction';
//import {fileUploadToServer} from '../action/fileUploadAction';
import FolderPane from '../components/folderpane/FolderPane';
import {highlightChildren, getFirstName} from '../components/common/browseAssetUtil';
import TreeNodeUtil from '../components/folderpane/TreeNodeUtil';


const getSelectedValues = (dataArray) => {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }
  return [];
}

const mapStateToProps = (state) => {
  let data = getSelectedValues(state.TreePaneReducers);
  //let data1 =getSelectedValues(state.SingleFileFolderReducer);
  return {
   model: data,
   //targetFolder:data1,
   //FolderID: data1.FolderID
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
     componentWillMount: function () {
       dispatch(getFolders());
      },
      componentDidUpdate : function (){
       //highlightChildren(getFirstName(this.props.model))
      },
     toggle:function (model,folderName,nodeRef){
        console.log('Folder Name : '+ folderName +' ... '+' Node Ref : '+nodeRef);


        //dispatch(fetchingAssets(nodeRef, 1, 'term=*'));
        dispatch(setReference(nodeRef));
      }
  }
}

const SingleFileFolderContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(FolderPane)

export default SingleFileFolderContainer;
