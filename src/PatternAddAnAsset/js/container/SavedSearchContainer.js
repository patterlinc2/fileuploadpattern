import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchSavedSearchData, checkBoxHandler,saveCheckedvalue } from '../action/savedSearchAction';
import SavedSearchComponent from '../components/searchLibrary/SavedSearchComponent';
import { getSavedSearchValues,
         getSearchProductItems,
         saveSearchValues,
         deleteSavedSearch,
         runSearch } from '../action/SearchLibraryAction';

import {DEFAULT_PAGE_NO,DEFAULT_SAVED_SEARCH_MAX_RESULTS} from '../constants/paginationConstants';
const getSelectedValues = (dataArray) => {
 if (dataArray) {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }
}
  return [];
}

const mapStateToProps = (state) => {
 let SavedSearchdata = getSelectedValues(state.savedSearchReducers);

  return {
    rows : SavedSearchdata.data.data,
    CheckedValues:SavedSearchdata.savedData,
    isChecked:SavedSearchdata.isChecked,
    enableDelete:SavedSearchdata.enableDelete,
    enableSearch:SavedSearchdata.enableSearch,
    pageDetails:SavedSearchdata.data.pageDetails
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
     componentWillMount() {
      dispatch(fetchSavedSearchData(DEFAULT_PAGE_NO,DEFAULT_SAVED_SEARCH_MAX_RESULTS));
    },
     onSelect: function onSelect(page, event) {
      event.preventDefault();
       dispatch(fetchSavedSearchData(page,DEFAULT_SAVED_SEARCH_MAX_RESULTS));
     },

    runSavedSearch(event){
    event.preventDefault();
    dispatch(runSearch());
    },
      deleteSavedSearch(event){
      event.preventDefault();
      dispatch(deleteSavedSearch());
   },

    handleChange(obj){
      event.preventDefault();
      dispatch(saveCheckedvalue(obj));
      dispatch(checkBoxHandler(obj));
    }
  };
}

const SavedSearchContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SavedSearchComponent)

export default SavedSearchContainer;
