import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {fetchingAssets} from '../action/assets';
import {getSearchProductItems,
        searchLibButtonVisibility} from '../action/SearchLibraryAction';
import {fetchSavedSearchData} from '../action/savedSearchAction';
import assetfilter from '../components/browse/assetfilters';
import serviceUrls from '../constants/service';
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';

const getSelectedValues = (dataArray) => {
  if (dataArray.size > 1) {
    let latestItem = dataArray.size-1;
    return dataArray.get(latestItem);
  }

  return [];
}

const mapStateToProps = (state) => {
   let data = getSelectedValues(state.assets);
   console.log(data.selectedIndex);
  return {
    selectedIndex:data.selectedIndex
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    tabHandleSelect: function (index, last) {
      sessionStorage.AssetTabIndex = index;
      let nodeRef;
      let filterUrlsForBrowse = {
        0:serviceUrls.allFilter,
        1:serviceUrls.imageFilter,
        2:serviceUrls.videoFilter,
        3:serviceUrls.audioFilter
      };
      nodeRef = document.querySelector('.filter-container .tree-node-selected');
      if (nodeRef) {
        let id = nodeRef.id;
        dispatch(fetchingAssets(id, DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS, filterUrlsForBrowse[index]));
      }
    }
  };
}

const AssetfilterContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(assetfilter)

export default AssetfilterContainer;
