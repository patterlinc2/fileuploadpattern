const request = require('superagent');
const  Promise = require('bluebird');
import serviceUrls from '../constants/service';

export default {

getRootChildren(token) {
    let url = serviceUrls.folderData+'&alf_ticket='+token;
    console.log(url);
    return Promise.promisifyAll(
          request.get(url)

        )
  }
}
