const request = require('superagent');
const  Promise = require('bluebird');
import serviceUrls from '../constants/service';


import { hashHistory } from 'react-router'

export default {

   check_ticket_valid(token){

   	return Promise.promisifyAll(
	      request
	     .get(serviceUrls.fileUploadUrl+'/login/ticket/'+token)
	     .auth('admin', 'admin')
	     )

   },
   get_token(){
		  return Promise.promisifyAll(
		  	request
	     .post(serviceUrls.fileUploadUrl+'/login')
	     .send({'username' : 'admin','password' : 'admin'})
		  )
   },

   get_Data() {

    return Promise.promisifyAll(
	      request
	     .get(serviceUrls.fileUploadUrl+'/login')
	     .query({ u: 'admin' })
	     .query({ pw: 'admin' })
	     //.auth('admin', 'admin')
	     .on('progress', function (e){
          //console.log(e.direction,'is done',e.percent,'%');
         })
     )
  },

   post_Data(formData,values,token) {

    //const filename = Date.now()+'_'+values.file[0].name;
    const filename = values.file[0].name;
	const Aflerco_Upload_Url = serviceUrls.fileUploadUrl+'/-default-/public/cmis/versions/1.1/browser/root?' +
	'objectId=workspace://SpacesStore/f732b1f5-5d54-4505-91dd-6ff1aeef5ff3' +
	'&cmisaction=createDocument&propertyId[0]=cmis:name&propertyValue[0]=' + filename +
	'&propertyId[1]=cmis:objectTypeId&propertyValue[1]=cmis:document&alf_ticket=' + token;


	       /*return Promise.promisifyAll(request
	      .post(Aflerco_Upload_Url)
	      .type('form')
	      .send(formData)
			.on('progress', function (e) {
			console.log('Percentage done: ', e.percent);
			//document.getElementById('p').value = e.percent;
			})
	      .end(function (err, res){
	            console.log('File upload Output:');
	            console.log( res );
	      }))*/


    return Promise.promisifyAll(
           request
           .post(Aflerco_Upload_Url)
           .send(formData)
    	)


  },
  file_Upload(formData,values,token,folderID,dispatch, getState){
    let _folderID = folderID;

    if(_folderID ==''){
		_folderID = '9bedf2ac-43c2-4409-809a-b87dd30c13a5'
    }
    console.info('_folderID:' + _folderID);

  	formData.append('containerid', '');
	formData.append('siteid', '');
	formData.append('uploaddirectory', '');
	formData.append('overwrite', '\"false\"');
	formData.append('destination', 'workspace://SpacesStore/'+_folderID);

  	const file_Upload_Url = serviceUrls.fileUploadUrl+'/upload';
    //console.log(dispatch +'-'+ getState);



	let apiData = {
	 rows :  [
	          {
	            Name: formData.get('filedata').name,
	            Size: 0+'kb',
	            Progress: 0,
	            status:'Uploading'
	          }
	      ]
	    }

		dispatch({
			type : 'JOB_STATUS',
			data : apiData
		})



    hashHistory.push('/CheckJobStatus');
  	return Promise.promisifyAll(
           request
           .post(file_Upload_Url)
           .auth('admin','admin')
           .send(formData)
           .on('progress', function (e){
            //console.log(e.direction,'is done',e.percent,'%');

			let state =  getState();
			let currstate = state.CheckJobStatusReducers[state.CheckJobStatusReducers.length-1];

			//console.log(currstate);

            //console.log(Math.round(e.percent));
            //console.log(e.target.readyState);
            //console.log(e.target.status);


			/*let progressData = {
			 rows :  [
			          {
			            Name: formData.get('filedata').name,
			            Size: Math.round(e.loaded/1024)+'kb',
			            Progress: Math.round(e.percent),
			            status:'Uploading'
			          }
			      ]
			   }*/

			  let progressData = {
		  	       rows :  [
		  	        {
		  			Name: formData.get('filedata').name,
		            Size: Math.round(e.loaded/1024)+'kb',
		            Progress: Math.round(e.percent),
		            status:'Uploading'
		           }
		          ]
			  }

              if(e.target.readyState == undefined && e.target.status == undefined){
            	  //currstate.rows[0] = progressData;
					dispatch({
					type : 'JOB_STATUS_UPDATE',
					data : progressData
					})

					 /*if(Math.round(e.percent) == 100){
						              console.log('COMPLETED');

             let completedData = {
		  	       rows :  [
		  	        {
		  			Name: currstate.rows[0].Name,
		            Size: currstate.rows[0].Size,
		            Progress: currstate.rows[0].Progress,
		            status:'Success'
		           }
		          ]
			  }

              		dispatch({
					type : 'JOB_STATUS_UPDATE',
					data : completedData
					})
					}*/

              }

              if(e.target.status == 200){
              	/*console.log(JSON.parse(e.target.responseText));
			    let result = JSON.parse(e.target.responseText);

			    let completeData = {
					  	       rows :  [
					  	        {
					  			Name: result.fileName,
					            Size: currstate.rows[0].Size,
					            Progress: 100,
					            status:'Success'
					           }
					          ]
						  }

              	dispatch({
					type : 'JOB_STATUS_UPDATE',
					data : completeData
					})
				*/
              }

			  //console.log(progressData);
			  //console.log(currstate);
           })
    	)

  }
}
