import assetsApi from '../api/assets';
import fileUploadApi from '../api/fileUploadApi';
import { DISPLAY_ASSETS, SEND_TO_QUAD} from '../constants/fileUploadConstants';
import {getAssetData} from '../components/common/browseAssetUtil';

let i = 1;
  export function fetchingAssets(nodeRef,pageNo,maxItems,
                              filter, sortValue,viewName){
    let index, limit;
    if(pageNo==1){
      index = 0;
      limit = index+maxItems;
    }else{
      index = (pageNo*maxItems)-maxItems;
      limit = index+maxItems-1;
    }

    return dispatch => {
      assetsApi.get_assets(nodeRef, index, limit, filter,sortValue).then(function (res){
        res.body.index = index;
        res.body.limit = limit;
        res.body.pageNo = pageNo;
        res.body.pageLimit = maxItems;
        res.body.showSaveSearch = false;
        res.body.displayItemCount = maxItems;
        if(viewName){
          res.body.viewName = viewName;
        }else{
          res.body.viewName = 'grid-view';
        }
        fileUploadApi.get_token().then(function (success){
          let  token = JSON.parse(success.text).data.ticket;
          res.body.token = token;
          let assetData = getAssetData(res.body);
          dispatch({
            type : DISPLAY_ASSETS,
            data : assetData
          });
        },function (error){
          console.log('fetching assets data:' + error);
        });
      },function (error){
       console.log('fetching assets data:' + error);
      })
  }
}

export function selectedRecord(record){
    return {
        type : SEND_TO_QUAD,
        data : record
    }
}

//Waiting for clarification for displaying image authentication.
// export function  gettingThumbnails (nodeRefs){
//     return dispatch => {
//         let data = assetsApi.getThumnailImages(nodeRefs);
//           dispatch({
//             type : DISPLAY_ASSETS,
//             data : data
//           })

//         }
// }
