import fileUploadApi from '../api/fileUploadApi'
import { Link, browserHistory, hashHistory } from 'react-router'

  export function fileUploadToServer(formData,values){

   return (dispatch, getState) => {
    	let state = getState();
    	let treePaneObj = {};
    	let currFolderId = '';
    	if (state.SingleFileFolderReducer.length > 0) {
		    treePaneObj = state.SingleFileFolderReducer[state.SingleFileFolderReducer.length-1];
		}
		if(treePaneObj){
			currFolderId = treePaneObj.FolderID
		}

		//console.log('current folder is ',currFolderId);
      fileUploadApi.get_token().then(function (success){

        //const parser = new DOMParser();
		//const xmlDoc = parser.parseFromString(success.text,'text/xml');
        //const token = xmlDoc.getElementsByTagName('ticket')[0].childNodes[0].nodeValue;
            const token = JSON.parse(success.text).data.ticket;
	         fileUploadApi.file_Upload(formData,values,token,currFolderId, dispatch, getState).then(function (success){

                   //let responseData = JSON.parse(success.xhr.responseText);
                   let responseData = JSON.parse(success.text);

                   //console.log(responseData)

	         		const apiData = {
				         rows :  [
				                  {
				                    //Name: responseData.properties['cmis:contentStreamFileName'].value,
				                    Name: responseData.fileName,
				                    Size: Math.round(formData.get('filedata').size/1024)+'kb',
				                    Progress: 100,
				                    status:'Success'
				                  }
				              ]
				            }
			        dispatch({
			          type : 'JOB_STATUS_END',
			          data : apiData
			        })
				//hashHistory.push('/UploadInProgress');

	         },function (error){
	           //console.log(error.message);
	            const apiData = {
				         rows :  [
				                  {
				                    Name: values.file[0].name,
				                    Size: '0kb',
				                    Progress: 0,
				                    status:'Failure'
				                  }
				              ]
				            }

                  dispatch({
			          type : 'JOB_STATUS_END',
			          data : apiData
			      })

	           //hashHistory.push('/errorUploading');

	         })


      },function (error){

       //console.log('Aflerco_Tocken_Error:' + error);
       //console.log(error.message);

       const apiData = {
				         rows :  [
				                  {
				                    Name: values.file[0].name,
				                    Size: '0kb',
				                    Progress: 0,
				                    status:'Failure'
				                  }
				              ]
				            }
        dispatch({
			          type : 'JOB_STATUS',
			          data : apiData
			      })

       hashHistory.push('/errorUploading');
      })
  }
}
