import assetsApi from '../api/TreePaneApi'
import fileUploadApi from '../api/fileUploadApi'
import { GET_FOLDER, TOGGLE,SET_REF} from '../constants/TreePaneConstant'
import {fetchingAssets} from '../action/assets';
import {getNodeRef,
        getFirstObj,
        highlightChildren,
        getFirstName,
        getNodeRefValue} from '../components/common/browseAssetUtil';
import JsonData from '../components/folderpane/TreeNodeUtil';
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';
import serviceUrls from '../constants/service';

let nodeRef;
  export function  getFolders(){
    return dispatch => {

      fileUploadApi.get_token().then(function (success){
        let token = JSON.parse(success.text).data.ticket;

      assetsApi.getRootChildren(token).then(function (res){
        let treeFolder = JsonData.getCustomFolder(res.body);
        // let treeFolder = JsonData.treeNodeUtil(res.body);
        // highlightChildren(treeFolder, getFirstName(treeFolder));
        dispatch({
          type : GET_FOLDER,
          data : treeFolder,
        })
        nodeRef = getNodeRef(getNodeRefValue(getFirstObj(treeFolder)));
        dispatch(fetchingAssets(nodeRef, DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS, serviceUrls.allFilter));

        //console.log(nodeRef);
        let curFolder = _.filter(treeFolder,'highlight')[0]['nodeRef'].split('/')[3];
        //console.log(curFolder);

        dispatch({
	      type: SET_REF,
	      data: curFolder
	    });


      },function (error){
       console.log('fetching child folders data:' + error);
      })

    },function (error){
       console.log('Aflerco_Tocken_Error:' + error);
       console.log(error.message);
     })
  }
  }

export function toggle(booValue, folderName){

return dispatch => {
      assetsApi.getFoldersService(folderName).then(function (res){
        dispatch({
          type : 'TOGGLE_TEST',
          data : res.body,
          toggle : booValue
        })
      },function (error){
       console.log('fetching child folders data:' + error);
      })
  }
}


export function setReference(nodeRef){
return dispatch => {
      dispatch({
          type : SET_REF,
          data : nodeRef

        }),
      console.log('folder id:' + nodeRef);
  }
}
