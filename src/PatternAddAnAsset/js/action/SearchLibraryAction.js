import searchLibraryApi from '../api/SearchLibraryApi';
import fileUploadApi from '../api/fileUploadApi';
import { SAVED_SEARCH_VALUE,SEARCH_DISPLAY_ASSETS,UPDATE_CHECKBOX_VALUE,SEARCH_BUTTON_VISIBILITY } from '../constants/searchLibraryConstants';
import { fetchSavedSearchData, checkBoxHandler } from '../action/savedSearchAction';
const localforage = require('localforage');
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';
import {AUTO_COMPLETE} from '../constants/searchLibraryConstants';
import serviceUrls from '../constants/service';

export function getSavedSearchValues() {
/*    return dispatch => {
     // searchLibraryApi.fetch_savedSearch_data().then(function (data) {
      searchLibraryApi.autoComplete_Data.then(function (data) {
        dispatch({
          type: CONST.SAVED_SEARCH_VALUE,
          SAVED_SEARCH_VALUE: JSON.parse(data.text)
        })
      }).catch(function (error){
          console.log(error);
      });
  }*/
}


export function getSearchProductItems(value,pageNo,maxItems, filter, sortOption,viewName){
  return dispatch => {
    let index, limit;
    if(pageNo==1){
      index = 0;
      limit = index+maxItems;
    }else{
      index = (pageNo*maxItems)-maxItems;
      limit = index+maxItems-1;
    }
    let selIndex;
    if(sessionStorage.AssetTabIndex){
      if(sessionStorage.AssetTabIndex==4){
        selIndex = 0;
      }else{
      selIndex = parseInt(sessionStorage.AssetTabIndex);
      }
    }else{
      selIndex = 0;
    }
      searchLibraryApi.getAssertsData(value,filter,index,limit, sortOption).then(function (res) {
        res.body.index = index;
        res.body.limit = limit;
        res.body.pageNo = pageNo;
        res.body.pageLimit = maxItems;
        res.body.SearchValue = value;
        res.body.showSaveSearch = true;
        res.body.selectedIndex = selIndex;
        res.body.displayItemCount = maxItems;
        if(viewName){
          res.body.viewName = viewName;
        }else{
          res.body.viewName = 'grid-view';
        }
        fileUploadApi.get_token().then(function (success){
          let  token = JSON.parse(success.text).data.ticket;
          res.body.token = token;
          dispatch({
            type: SEARCH_DISPLAY_ASSETS,
            data: res.body
          })
        })
       })
    }
  }


export function saveSearchValues(value){

  return dispatch => {
     // searchLibraryApi.fetch_savedSearch_data().then(function (data) {
      searchLibraryApi.saveSearchValue(value).then(function (data) {
        /*dispatch({
          type: DISPLAY_ASSETS,
          data: JSON.parse(data.text)
        })*/
      })
  }

}

export function getUpdatedCheckboxValue(enableDelete, enableSearch){
  return (dispatch) => {
        dispatch({
          type: UPDATE_CHECKBOX_VALUE,
          checkBoxUpdate:{'enableDelete':enableDelete,'enableSearch':enableSearch}
        })
  }
}

export function searchLibButtonVisibility(isSavedSearch){
   return (dispatch) => {
        dispatch({
          type: SEARCH_BUTTON_VISIBILITY,
          isSavedSearch:{'isSavedSearch':isSavedSearch}
        })
  }
}

export function deleteSavedSearch(){

  return (dispatch, getState) => {
    localforage.getItem('savedSearch', function (err, res) {
      console.log(res);
      let state = getState();
      let savedSearchData = state.savedSearchReducers[0].savedData
      console.log(savedSearchData.length);
      let filteredData = [],deletedData=[];
       for(let i=0;i<res.length;i++){
        for(let j=0;j<savedSearchData.length;j++){
          if(res[i].id == savedSearchData[j].id){
            if(savedSearchData[j].checked==true){
              res[i].isChecked = true;
            }
          }
        }
       }
       for(let i=0;i<res.length;i++){
        if(res[i].isChecked==false){
          filteredData.push(res[i]);
        }else{
          deletedData.push(res[i]);
        }
       }
       console.log(deletedData);
       dispatch({
            type: 'DELETE_CHECKED_SAVED_SEARCH_VALUE',
            data: deletedData
          })
       if(localforage.setItem('savedSearch',filteredData)){
       localforage.getItem('savedSearch', function (err, res) {
       dispatch(fetchSavedSearchData(DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS));
        });
      }
    });
    // let state = getState();
    //   let savedSearchData = state.savedSearchReducers[0].data.data;
    //   let filteredData = [];
    //   savedSearchData.map(function (item){
    //       if(item.isChecked===false){
    //         filteredData.push(item);
    //       }
    //   });
//       localforage.clear(function (err) {
//     // Run this code once the database has been entirely deleted.
//     console.log('Database is now empty.');
//           dispatch(fetchSavedSearchData(1));
// });
      //localforage.clear(function (err) {
      // debugger;
      // localforage.setItem('savedSearch',filteredData);
      // dispatch(fetchSavedSearchData(1));
      //});
      /*dispatch({
          type: 'SAVED_SEARCH_GET',
          Search_Data : filteredData,
          success: true
      })*/
      //dispatch(fetchSavedSearchData());
  }

}

export function runSearch(){
    return (dispatch, getState) => {
    let state = getState();
      let savedSearchData = state.savedSearchReducers[0].savedData;
      savedSearchData.map(function (item){
          if(item.checked===true){
            document.querySelector('#addAnAssets .react-autosuggest__input').value = item.name;
            let prevValue = state.autoComplete[state.autoComplete.length-1];
              // state.autoComplete.push({'text':item.name,
              //                          'data':prevValue.data,
              //                          'lastThreeSearch':prevValue.lastThreeSearch,
              //                          'savedSearch':prevValue.savedSearch
              //        });
                   //let check = document.querySelector('#searchButton').click();
                   //console.log(check);
            //dispatch(getSearchProductItems(item.name,DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS));
                    dispatch({
          type: AUTO_COMPLETE,
          data: prevValue.data,
          text: item.name,
          savedSearch: prevValue.savedSearch,
          lastThreeSearch: prevValue.lastThreeSearch
        });
        dispatch(getSearchProductItems(item.name,DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS,serviceUrls.searchBasedAll));

          }
      });
  }
}
