/*
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *.
 *
 * LinkMetaDataActions
 */
import SavedSearchApi from '../api/savedSearchApi'
import { getUpdatedCheckboxValue } from './SearchLibraryAction'
import { CHECKED_SAVED_SEARCH_VALUE} from '../constants/searchLibraryConstants';

//let maxItems=4;
  export function  fetchSavedSearchData(pageNo,maxItems){
      let index = (pageNo*maxItems)-maxItems;
     let limit = index+maxItems;
    return dispatch => {
      SavedSearchApi.get_Saved_Search_Data(index,limit).then(function (data){
        let FilteredData = [];
        for(let i=index;i<limit;i++){
          if(data[i]){
          FilteredData.push(data[i]);
          }
        }
       // if(FilteredData.length>0){
        FilteredData.numberFound = data.length;
        let savedSearchData = {
          'data':FilteredData,
          'pageDetails':{
            'pageNo':pageNo,
            'index':index,
            'limit':limit,
            'pageLimit':maxItems,
            'numberFound':data.length,
            'totalRecords':FilteredData.length
          }
        }
        dispatch({
          type: 'SAVED_SEARCH_GET',
          Search_Data : savedSearchData,
          success: true,
          isSavedSearch:true
        })
      //}
      }).catch(e => {

        console.log('error: '+ e) ;
        dispatch({
          type: 'METADATA_GET_ERROR',
          QMD_Data_Err : e.message,
          success: false
        })
    })
  }

}

export function checkBoxHandler(obj){
 /* console.log('In checkBoxHandler action');
   return dispatch({
          type: 'CHECKBOX_HANDLER',
          checkTest: checked
  })*/

   return (dispatch, getState) => {
    obj.callback = function (enableDelete, enableSearch){
          console.log('The call back function');
          //call searchlibrary action to set
        //dispatch(getUpdatedCheckboxValue(enableDelete, enableSearch));
        let state = getState();
        state.searchLibraryReducer.enableDelete = enableDelete;
        state.searchLibraryReducer.enableSearch = enableSearch;
    }
      console.log('In checkBoxHandler action: '+obj);
        dispatch({
         type: 'CHECKBOX_HANDLER',
         checkTest:obj
        })

  }
}

export function saveCheckedvalue(obj){
  return {
        type : 'CHECKED_SAVED_SEARCH_VALUE',
        data : obj
    }
}
