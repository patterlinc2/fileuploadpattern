
import searchLibraryAction from '../api/SearchLibraryApi'
import {AUTO_COMPLETE} from '../constants/searchLibraryConstants';


export function populateAutoComplete(text,savedSearch,lastThreeSearch) {
    return dispatch => {
    	searchLibraryAction.autoComplete_Data(text).then(function (data){
        dispatch({
          type: AUTO_COMPLETE,
          data: JSON.parse(data.text),
          text,
          savedSearch,
          lastThreeSearch
        });
    });

  }
}
