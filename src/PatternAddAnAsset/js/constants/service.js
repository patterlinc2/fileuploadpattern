 const testUrl = 'http://localhost:3000/folderService';
// const baseUrl = 'http://10.219.43.96:8082/alfresco'
const baseUrl = 'http://10.219.43.16:8080/alfresco'
// const baseUrl = 'http://localhost:8080/alfresco'

const ticket = '';

const service = {
	questionMetaData: baseUrl + 'questionMetaData',
	saveQuestionMetaData: baseUrl + 'saveQuestionMetaData',
    assetsData: baseUrl + 'api/-default-/public/cmis/versions/1.1/browser/root/Senthil?',
    folderData: baseUrl + '/api/-default-/public/cmis/versions/1.1/browser/root?folderId=&cmisselector=descendants&depth=-1',
    testFolder: testUrl,
    thumbnail: baseUrl + '/service/api/node/workspace/SpacesStore/{0}/content/thumbnails/doclib',
    getAssets: baseUrl + '/service/slingshot/search?repo=true&sort={2}&rootNode=workspace://SpacesStore/{0}&{1}',
    imageFilter: 'filters={http://www.alfresco.org/model/content/1.0}content.mimetype|image/*&term=*',
    videoFilter: 'filters={http://www.alfresco.org/model/content/1.0}content.mimetype|video/*&term=*',
    audioFilter: 'filters={http://www.alfresco.org/model/content/1.0}content.mimetype|audio/*&term=*',
    allFilter: 'term=* AND -TYPE:"cm:folder"',
    defaultThumbVideo: baseUrl + '/service/api/node/workspace/SpacesStore/ce01cdd9-34f9-43f1-a992-b4da551e6115/content/thumbnails/doclib?c=queue&ph=true',
    defaultThumbAudio: baseUrl + '/service/api/node/workspace/SpacesStore/a184bef6-4f26-42a0-8b9d-fc2b87f2d639/content/thumbnails/doclib?c=queue&ph=true',
    defaultThumbDoc: './images/doc-thumbnail.png',
    listSearchBasedAssets: baseUrl + '/service/slingshot/search?term={0}&repo=true&startIndex=0&maxResults=100',
    searchBasedImage: baseUrl + '/service/slingshot/search?filters={http://www.alfresco.org/model/content/1.0}content.mimetype|image/*&term={0}&sort={1}&rootNode=alfresco//Fcompany/home&repo=true',
    searchBasedVideo: baseUrl + '/service/slingshot/search?filters={http://www.alfresco.org/model/content/1.0}content.mimetype|video/*&term={0}&sort={1}&rootNode=alfresco//Fcompany/home&repo=true',
    searchBasedAudio: baseUrl + '/service/slingshot/search?filters={http://www.alfresco.org/model/content/1.0}content.mimetype|audio/*&term={0}&sort={1}&rootNode=alfresco//Fcompany/home&repo=true',
    searchBasedAll: baseUrl + '/service/slingshot/search?term={0}&sort={1}&rootNode=alfresco//Fcompany/home&repo=true',
    fileUploadUrl : baseUrl+'/service/api',
    Alfresco_Search_Url: baseUrl+'/service/slingshot/auto-suggest?'
}


module.exports = service;
